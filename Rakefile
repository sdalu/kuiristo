# -*- ruby -*-
$LOAD_PATH << "#{__dir__}/lib"

require 'bundler/setup'

# Load kuiristo requirements
require 'kuiristo'
require 'kuiristo/config'
require 'kuiristo/cli'
$config.dig(:sources)&.each_key {|source|
require "kuiristo/source/#{source}"
}


# Logger
$logger   = Logger.new($stdout, level: $config.dig(:logger, :level))
$logger.formatter = proc {|severity, datetime, progname, msg|
    str = case msg
          when String     then msg
          when Exception  then "#{msg.message} (#{msg.class})\n" <<
                               (msg.backtrace || []).join("\n")
          else                   msg.inspect
          end
        
    "%s [%s]: %s\n" % [ severity[0..0],
                        datetime.strftime('%Y-%m-%d %H:%M:%S'),
                        str
                      ]
}

# Database connector
$db = DB  = Sequel.connect({ :encoding  => 'utf8',
                             :adapter   => 'mysql2'
                           }.merge($config.dig(:db)))

# Command line
$kuiristo = Kuiristo::CLI.new($config.dig(:sources), DB,
                              :parser  => $config.dig(:parser),
                              :proxy   => $config.dig(:proxy),
                              :fetcher => $config.dig(:fetcher),
                              :logger  => $logger)


# Load rake rules for kuiristo
require 'kuiristo/rake/db'
require 'kuiristo/rake/web'
require 'kuiristo/rake/fetch'
require 'kuiristo/rake/debug'
require 'kuiristo/rake/list'




desc "Import recipes (fetch, build, tag)"
task "import", [:source] do |t, args|
    [ 'fetch',
      'db:build',
      'db:autotag',
    ].each {|name|
        Rake::Task[name].invoke(args[:source])
    }
end
