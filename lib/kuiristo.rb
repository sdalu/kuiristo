# coding: utf-8

require 'set'
require 'nokogiri'
require 'kuiristo/core_extensions'

module Kuiristo
    class Error                 < StandardError ; end
    class DBError               < Error         ; end
    class NotFound              < Error         ; end

    NS_PREFIX      = 'k'
    NS_URI         = 'urn:kuiristo'

    ROBOT_LIST     = [ 'TM5', 'TM21', 'TM31' ]

    # From solarized pallette
    #   http://ethanschoonover.com/solarized
    COLOR_PALETTE = {
        :colors => {
            :yellow  => '#b58900',    :orange  => '#cb4b16',
            :red     => '#dc322f',    :magenta => '#d33682',
            :violet  => '#6c71c4',    :blue    => '#268bd2',
            :cyan    => '#2aa198',    :green   => '#859900',
        },
        :monotones => {
            :light => [ '#fdf6e3', '#eee8d5', '#93a1a1', '#839496',
                        '#657b83', '#586e75', '#073642', '#002b36' ],
            :dark  => [ '#002b36', '#073642', '#586e75', '#657b83',
                        '#839496', '#93a1a1', '#eee8d5', '#fdf6e3' ],
        },
        :roles => {
            :light => {
                :content_emphazied     => '#93a1a1',
                :content_primary       => '#839496',
                :content_secondary     => '#586e75',
                :background_highlights => '#073642',
                :background            => '#002b36',
            },            
            :dark =>  {
                :content_emphasized    => '#586e75',
                :content_primary       => '#657b83',
                :content_secondary     => '#93a1a1',
                :background_highlights => '#eee8d5',
                :background            => '#fdf6e3',
            },
        },
    }
    
    

    SOURCES_BY_NICKNAME = {}
    SOURCES_BY_STRING   = {}
    SOURCES_BY_ID       = {}
    SOURCES             = Set.new
    
    def self.register_source(mod)
        nickname, id = mod::NICKNAME, mod::ID
        if SOURCES.include?(mod)
            raise "source already registered"
        end
        if SOURCES_BY_NICKNAME.has_key?(nickname)
            raise "nickname <#{nickname}> already registered"
        end
        if SOURCES_BY_ID.has_key?(id)
            raise "id <#{id}> already registered"
        end

        SOURCES_BY_NICKNAME[nickname   ] = mod
        SOURCES_BY_STRING[nickname.to_s] = mod
        SOURCES_BY_ID[      id         ] = mod
        SOURCES                         << mod
    end

    # Get the source module from one of its identifier
    #
    # @param [Module, Symbol, Integer, String] identifier
    # @return [Module] Source module
    def self.source(id)
        mod = case id
              when Module  then SOURCES.include?(id) ? id : nil
              when Symbol  then SOURCES_BY_NICKNAME[id     ]
              when Integer then SOURCES_BY_ID[      id     ]
              when /^\d+$/ then SOURCES_BY_ID[      id.to_i]
              when String  then SOURCES_BY_STRING[  id     ]
              else raise ArgumentError,
                     "expecting symbol, integer, string or module as argument"
              end
        if mod.nil?
            raise "requested source has not been registered (#{id})"
        end
        mod
    end

    # Sorted by order to display for ordering ingredients
    UNITS = {
        :unit        =>[ nil,                 nil,                 nil, :qt ],
        :jar         =>[ 'pot',               'pots',              :de      ],
        :pack        =>[ 'paquet',            'paquets',           :de      ],
        :tin         =>[ 'conserve',          'conserves',         :de      ],
        :bottle      =>[ 'bouteille',         'bouteilles',        :de      ],
        :sachet      =>[ 'sachet',            'sachets',           :de      ],
        :dose        =>[ 'dose',              'doses',             :de      ],
        :glass       =>[ 'verre',             'verres',            :de      ],
        
        :piece       =>[ 'morceau',           'morceaux',          :de      ],
        :piece_small =>[ 'petit morceau',     'petits morceaux',   :de      ],

        :gramme      =>[ 'g',                 'g',                 :de, :pl ],
        :kg          =>[ 'kg',                'kg',                :de, :pl ],
        :centimeter  =>[ 'cm',                'cm',                :de      ],
        :cl          =>[ 'cl',                'cl',                :de      ],
        :litre       =>[ 'litre',             'litres',            :de      ],

        :sp          =>[ 'c. à soupe',        'c. à soupes',       :de      ],
        :sp_level    =>[ 'c. à soupe rase',   'c. à soupe rases',  :de      ],
        :sp_heaped   =>[ 'c. à soupe bombée', 'c. à soupe bombées',:de      ],
        :tsp         =>[ 'c. à café',         'c. à cafés',        :de      ],
        :tsp_heaped  =>[ 'c. à café bombée',  'c. à café bombées', :de      ],
        :tsp_level   =>[ 'c. à café rase',    'c. à café rases',   :de      ],
        :pinch       =>[ 'pincée',            'pincées',           :de      ],
        :kniff_tip   =>[ 'pointe de couteau', 'pointes de couteau',:de      ],
        :streak      =>[ 'trait',             'traits',            :de      ],
        :some        =>[ 'quelques',          'quelques',          nil, :pl ],

        :slice       =>[ 'tranche',           'tranches',          :de      ],

        :branch      =>[ 'branche',           'branches',          :de      ],
        :bouquet     =>[ 'bouquet',           'bouquets',          :de      ],
        :leaf        =>[ 'feuille',           'feuilles',          :de      ],
        :leaf_small  =>[ 'petite feuille',    'petites feuilles',  :de      ],
        :stick       =>[ 'bâton',             'bâtons',            :de      ],
        :cube        =>[ 'cube',              'cubes',             :de      ],
        :ball        =>[ 'boule',             'boules',            :de      ],
        :sprig       =>[ 'brin',              'brins',             :de      ],
        :fillet      =>[ 'filet',             'filets',            :de      ],
        :drop        =>[ 'goutte',            'gouttes',           :de      ],
    }

    def self.normalize_quantity_unit(quantity, unit)
        case unit
        when :kg
            quantity *= 1000
            unit      = :gramme
        when :litre
            quantity *= 100
            unit      = :cl
        end
        [ quantity, unit ]
    end

    def self.denormalize_quantity_unit(quantity, unit)
        # Convert to higher unit multiplier
        # Allows: 1/4, 1/2, 3/4 and x.d (x > 1)
        # Returns nil if not possible
        num_cnv = ->(v, factor) {
            if [ Rational(1,4), Rational(2,4), Rational(3,4)
               ].map{|r| r * factor }.include?(v) ||
               ( (v >= factor) && (v % (factor / 10).to_i == 0) )
                v  / factor.to_f
            end
        }
        
        # Convert quantity to higher unit mulitplier
        # Returns nil if not possible
        qty_cnv = ->(qty, factor) {
            case qty
            when Numeric
                num_cnv.(qty, factor)
            when Range
                a = num_cnv.(qty.begin, factor)
                b = num_cnv.(qty.end,   factor)
                a..b if a && b  # is nil if a.nil? || b.nil?
            else raise ArgumentError,
                       "unsupported quantity type (#{quantity.class})"
            end
        }

        case unit
        when :gramme
            if qty = qty_cnv.(quantity, 1000)
                quantity = qty
                unit     = :kg
            end
        when :cl
            if qty = qty_cnv.(quantity, 100)
                quantity = qty
                unit     = :litre
            end
        end

        [ quantity, unit ]
    end


    
    # Returns a pluralized unit name
    #
    # @param nickname [Symbol]
    # @param qty [Numeric, Range, nil] quantity
    # @return [String] pluralized unit name
    def self.unit_name(nickname, qty=nil)
        if !UNITS.include?(nickname)
            raise ArgumentError, "unknown unit (#{nickname})"
        end

        singular, plural = UNITS[nickname]
        qty = case qty
              when Numeric  then qty
              when Range    then [ qty.first, qty.last ].max
              when nil      then nil
              else raise ArgumentError
              end
        qty&.>=(2) ? plural : singular
    end

    # Returns the singular or plural form, eventually prefixed,
    # according to the unit and quantity it will be used with.
    #
    # @param singular [String] singular form
    # @param plural [String] plural form
    # @param unit [Symbol] unit name to be used with
    # @param qty [Numeric, Range, nil] quantity to be used with
    # @return [String] singular or plural form
    def self.ingredient_notation(singular, plural, unit, quantity)
        singular ||= plural
        plural   ||= singular
        
        return nil if singular.nil? && plural.nil?
        
        unless desc = UNITS[unit]
            raise ArgumentError, "unknown unit (#{unit})"
        end
        
        _, _, prefix, number = desc
        
        qmax = case quantity
               when Numeric then quantity
               when Range   then [ quantity.first, quantity.last ].max
               when nil     then nil
               else raise ArgumentError,
                          "unsupported quantity ({#quantity})"
               end

        name = case number
               when :sg, nil then singular
               when :pl      then plural
               when :qt      then qmax&.>=(2) ? plural : singular
               else raise ScriptError, "unsupported number form"
               end

        if prefix == :de
            name = ((name =~ /^[haeéêèioœu]/i) ? "d'" : "de ") + name
        end
            
        name
    end

    # Return a downcase transcoded ASCII string
    #
    # @note Normalizing using :nkfd is not enough due to oe
    #
    # @param str [String] an UTF-8 string
    # @return [String] an ASCII string
    def self.normalize_string_ci(str)
        str.downcase
           .unicode_normalize(:nfkd).gsub("\u0153", 'oe')
           .encode('ASCII', replace: '')
    end

    # Transform the specfics namespaced element to plain html
    # 
    # @param  [String,
    #          Nokogiri::XML::Document,  Nokogiri::XML::DocumentFragment
    #          Nokogiri::HTML::Document, Nokogiri::HTML::DocumentFragment
    #         ] input data as string or Nokogiri document
    # @return [String] html formated string
    def self.to_html(data, **o)
        doc = case data
              when Nokogiri::XML::Document, Nokogiri::XML::DocumentFragment
                  data
              when String
                  Nokogiri::HTML.fragment(data)
              else
                  raise ArgumentError, "unexpected data of type #{data.class}"
              end

        doc.css('tts').each {|n|  # xpath('//tts') doesn't seems to work
            h    = Hash[n.attributes.map {|k,v| [ k.to_sym, v.to_s ] }]
            tts  = Recipe::TTS.from_hash(h)
            html = Nokogiri::HTML.fragment(tts.to_html)
            n.replace(html)
        }
        doc.to_html
    end

    # Format
    # @param format [:string, :json, :display]
    def self.value_to(value, format=:string, opts={:nil => ""})
        to_s = ->(v) {
            case v
            when BigDecimal then v.frac.zero? ? v.to_i.to_s : v.to_s('F')
            when Float      then (v.to_i == v) ? v.to_i.to_s : v.to_s
            when Numeric    then v.to_s
            else raise ArgumentError, "unsupported value type (#{v.class})"
            end
        }

        case value
        when Range    then
            v = (to_s.(value.begin)) + '..' + (to_s.(value.end))
            v = '"'+v+'"' if format == :json
            v
        when Numeric  then to_s.(value)
        when NilClass then (format == :json) ? "null" : opts[:nil]
        else raise ArgumentError, "unsupported value type (#{value.class})"
        end
    end



    def self.wait(delay, &info_cb)
        if wait = case delay
                  when nil, 0  then nil
                  when Integer then delay
                  when Range   then rand(delay)
                  end
            info_cb&.(wait)
            sleep(wait)
        end
    end

    def self.hashnick(h)
        "%s<%s>%s" % [
            h[ 0.. 2].unpack('H*').first,
            Digest::MD5.hexdigest(h)[0..1],
            h[-3..-1].unpack('H*').first,
        ]
    end


    STOPWORDS  = [ 'de', 'à', 'au', 'aux', 'des', 'en', 'd\'',
                   'avec', 'sans', 'pour', 'ou', 'la', 'le', '', 'type' ]

    def self.search_breaker(*sentences)
        sentences
            .flat_map {|s| s.split(/\s+|(?<=d\')/) }
            .reject   {|w| STOPWORDS.include?(w) }
            .map      {|w| Kuiristo.normalize_string_ci(w) }
    end

end


require_relative 'kuiristo/recipe'
