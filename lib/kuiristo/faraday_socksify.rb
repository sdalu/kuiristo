require 'socksify'
require 'socksify/http'

# Socksify::debug = true

class Faraday::Adapter::NetHttp
    def net_http_connection(env)
        if proxy = env[:request][:proxy]
            if proxy[:uri].scheme.start_with?('socks')
                TCPSocket.socks_username = proxy[:user]
                TCPSocket.socks_password = proxy[:password]
                Net::HTTP::SOCKSProxy(proxy[:uri].host, proxy[:uri].port)
            else
                Net::HTTP::Proxy(proxy[:uri].host, proxy[:uri].port,
                                 proxy[:user], proxy[:password])
            end 
        else
            Net::HTTP
        end.new(env[:url].host, env[:url].port ||
                                (env[:url].scheme == 'https' ? 443 : 80))
    end
end

