# coding: utf-8

require 'bigdecimal'


module Kuiristo

module StringFormatter
    refine String do
        def truncate(truncate_at, ellipsis: '...', separator: nil)
            return dup unless length > truncate_at
            
            length_with_room_for_ellipsis = truncate_at - ellipsis.length
            stop = if separator
                       rindex(separator, length_with_room_for_ellipsis) || length_with_room_for_ellipsis
                   else
                       length_with_room_for_ellipsis
                   end
            
            "#{self[0, stop]}#{ellipsis}"
        end
    end
end


module RangeArithmetic
    refine Range do
        # Add range and/or numeric together
        #
        # @param o [Range, Numeric]
        # @return  [Range, Numeric]
        def +(o)
            unless (Numeric === self.begin) && (Numeric === self.end)
                raise ArgumentError,
                      "range object need to be of numeric values"
            end
            if self.begin > self.end
                raise ArgumentError, "unordered range object"
            end

            r = case o
                when Range
                    unless (Numeric === o.begin) && (Numeric === o.end)
                        raise ArgumentError,
                              "range argument need to be of numeric values"
                    end
                    if o.begin > o.end
                        raise ArgumentError, "unordered range argument"
                    end
                    (self.begin + o.begin) .. (self.end + o.end)
                when Numeric
                    (self.begin + o) .. (self.end + o)
                else raise ArgumentError,
                           "argument need to be range or numeric"
                end
            (r.begin == r.end) ? r.begin : r
        end

        def ==(o)
            (Range === o) ? super(o) : super(o..o)
        end

        def *(o)
            unless (Numeric === self.begin) && (Numeric === self.end)
                raise ArgumentError,
                      "range object need to be of numeric values"
            end
            unless Numeric === o
                raise ArgumentError, "argument need to be numeric"
            end
            (self.begin * o) .. (self.end * o)
        end
    end

    refine BigDecimal do
        def ==(o)
            (Range === o) ? (o == self) : super(o)
        end
        
        def +(o)
            (Range === o) ? (o + self) : super(o)
        end
    end

    refine Integer do
        def ==(o)
            (Range === o) ? (o == self) : super(o)
        end

        def +(o)
            (Range === o) ? (o + self) : super(o)
        end
    end

    refine Float do
        def ==(o)
            (Range === o) ? (o == self) : super(o)
        end

        def +(o)
            (Range === o) ? (o + self) : super(o)
        end
    end
end
    
module ValueDisplay
    refine BigDecimal do
        def to_display
            if frac = case self.frac
                      when FRAC_25 then '¼'
                      when FRAC_50 then '½'
                      when FRAC_75 then '¾'
                      end
                if self.to_i.zero?
                then frac
                else "#{self.to_i}#{frac}"
                end
            elsif self.frac.zero?
                self.to_i.to_s
            else
                self.to_s('F')
            end
        end
    end

    
    refine Float do
        def to_display
            if frac = case self.abs.modulo(1)
                      when 0.25 then '¼'
                      when 0.5  then '½'
                      when 0.75 then '¾'
                      when 0.0  then nil
                      end
                if self.to_i.zero?
                then frac
                else "#{self.to_i}#{frac}"
                end
            elsif frac.nil?
                self.to_i.to_s
            else
                self.to_s
            end
        end
    end

    
    refine Range do
        def to_display
            self.begin.to_display + ' - ' + self.end.to_display
        end
    end

    
    refine Object do
        def to_display
            self.to_s
        end
    end
    
    private
    
    FRAC_50 = BigDecimal('0.50')
    FRAC_25 = BigDecimal('0.25')
    FRAC_75 = BigDecimal('0.75')
end
end
