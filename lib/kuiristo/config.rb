require 'yaml'

# Load configuration
$config = YAML.load_file('config.yaml', aliases: true)

# Load extra requirements
$config[:require]&.each {|file|
    require file
}

#
# Normalize
#

# Proxy
if Array === $config.dig(:proxy, :uri)
    $config[:proxy][:uri] = $config.dig(:proxy, :uri).sample
end

# Source
$config.dig(:sources)&.reject! {|source, disabled: false, **| disabled }

# Web / Index
if wi = $config.dig(:web, :index)
    # srcset
    if cfg = wi.dig(:srcset)
        sizes, min, max = Array(cfg)
        limit           = (min && max) ? (min..max) : nil
        cfg[:srcset] = [ sizes, limit ]
    end

    [ :header, :footer, :index, :view, :search ].each {|k|
        case cfg = wi.dig(k)
        when false, true then wi[k] = { :visible => cfg }
        when Symbol      then wi[k] = { :visible => true, cfg => true }
        end
    }
end




# Sanity check
if $config.dig(:web, :index, :srcset   ).nil? &&
   $config.dig(:web, :index, :img_score).nil?
    warn "config.yaml: consider defining :srcset or :img_score in :web, :index:"
end
