desc "List ingredient categories"
task "list:categories:ingredient" do
    puts "%4s | %-13s | %-20s | %s" % [ 'ID', 'Group', 'Key', 'Name' ]
    puts [5, 15, 22, 30].map {|c| '-' * c}.join('+')
    DB[:categories_for_ingredients].order(:group, :id).each {|cat|
        puts "%4{id} | %-13{group} | %-20{key} | %{name}" % cat
    }
end

desc "List recipe categories"
task "list:categories:recipe" do
    puts "%4s | %-13s | %-20s | %s" % [ 'ID', 'Group', 'Key', 'Name' ]
    puts [5, 15, 22, 30].map {|c| '-' * c}.join('+')
    DB[:categories_for_recipes].order(:group, :id).each {|cat|
        puts "%4{id} | %-13{group} | %-20{key} | %{name}" % cat
    }
end


desc "List sources"
task "list:sources" do
    puts "%4s | %-16s | %s" % [ 'ID', 'Nickname', 'Name' ]
    puts "-----+------------------+-----------------------------------"
    Kuiristo::SOURCES.sort {|a,b| a::ID <=> b::ID }.each {|s|
        puts "%4i | %-16s | %s" % [s::ID, s::NICKNAME, s::NAME]
    }
end


desc "List ingredients"
task "list:ingredients" do
    puts "%5s | %15s | %s" % [ 'ID',  'Profil', 'Name' ]
    puts "------+-----------------+-----------------------------------"
    DB[:ingredients]
        .select(:id,
                :profil,
                Sequel.function(:coalesce, :singular, :plural).as(:fullname))
        .each { |row|
        puts "%5{id} | %15{profil} | %{fullname}" % row
    }    
end
