
desc "Fetch index"
task "fetch:index", [:source] do |t, args|
    sources = Array(args[:source])
    sources = Kuiristo::SOURCES.map{|s| s::NICKNAME } if sources.empty?
    sources.each {|s| $kuiristo.fetch_index(s) }
end



desc "Fetch recipe"
task "fetch:recipe", [:recipe] do |t, args|
    source, id = $kuiristo.parse_source_recipe(args[:recipe],
                                            want: :rawid,
                                         allowed: [ :no_recipe, :nil ])
    if id
    then $kuiristo.fetch_recipe(source, id)
    else $kuiristo.fetch_recipes(source)
    end
end



desc "Fetch recipe dependencies"
task "fetch:dependencies", [:recipe] do |t, args|
    source, id = $kuiristo.parse_source_recipe(args[:recipe],
                                            want: :rawid,
                                         allowed: [ :no_recipe, :nil ])
    if id
    then $kuiristo.fetch_recipe_dependencies(source, id)
    else $kuiristo.fetch_recipes_dependencies(source)
    end
end


desc "Fetch index/recipe/dependencies"
task "fetch", [:source] do |t, args|
    [ 'fetch:index',
      'fetch:recipe',
      'fetch:dependencies',
    ].each {|name|
        Rake::Task[name].invoke(args[:source])
    }
end
