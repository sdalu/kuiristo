desc "Start Kuiristo web server"
task "web:start", [:debug] do |t, args|
    cargs   = [ '-I', 'lib' ]
    
    if    host   = $config.dig(:web, :host)
        cargs << '-o' << host.to_s
        if port = $config.dig(:web, :port)
            cargs << '-p' <<  port.to_s
        end
    elsif socket = $config.dig(:web, :socket)
        cargs << '-o' << socket
    end
    
    if args[:debug]
    then sh('shotgun', *cargs)
    else sh('rackup', '-E', 'production', *cargs)
    end
end

