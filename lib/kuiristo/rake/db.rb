Sequel.extension :migration
DB.extension :schema_dumper


######################################################################
# Database initialistation/migration                                 #
######################################################################

desc "Init database"
task "db:init" => [ "db:migrate", "db:import" ] do
end



desc "Run migrations"
task "db:migrate", [:version] do |t, args|
    if args[:version]
        puts "Migrating to version #{args[:version]}"
        Sequel::Migrator.run(DB, "data/db/migrations",
                             target: args[:version].to_i)
    else
        puts "Migrating to latest"
        Sequel::Migrator.run(DB, "data/db/migrations")
    end
end




######################################################################
# Building data                                                      #
######################################################################

desc "Build recipe"
task "db:build:recipe", [:recipe] do |t, args|
    source, id = $kuiristo.parse_source_recipe(args[:recipe],
                                            want: :rawid,
                                         allowed: [ :no_recipe, :nil ])
    if id
    then $kuiristo.db_build_xml_recipe(source, id)
    else $kuiristo.db_build_xml_recipes(source)
    end
end



desc "Build ingredients index"
task "db:build:index:ingredients", [:source] do |t, args|
    $kuiristo.db_build_index_ingredient(args[:source])
end



desc "Autotag recipes"
task "db:autotag", [:recipe] do |t, args|
    source, id = $kuiristo.parse_source_recipe(args[:recipe],
                                            want: :id,
                                         allowed: [ :no_recipe, :nil ])
    if id
    then $kuiristo.db_autotag_recipe(source, id)
    else $kuiristo.db_autotag_recipes(source)
    end
end



desc "Compute images phash"
task "db:build:image:phash" do
    $kuiristo.db_build_all_image_phash
end



desc "Build recipe/ingredient/image"
task "db:build", [:source] do |t, args|
    [ 'db:build:recipe',
      'db:build:index:ingredients',
      'db:build:image:phash',
    ].each {|name|
        Rake::Task[name].invoke(args[:source])
    }
end




######################################################################
# Removing data                                                      #
######################################################################

desc "Purge database"
task "db:purge" do
    $kuiristo.db_purge
end


desc "Remove blacklisted recipes"
task "db:remove:blacklisted", [ :source ] do |t, args|
    $kuiristo.db_remove_blacklisted(args[:source])
end


desc "Remove orphean pictures"
task "db:remove:orphean-pictures" do |t, args|
    $kuiristo.db_remove_orphaned_images
end




######################################################################
# Marking data                                                       #
######################################################################

desc "Mark recipes for reprocessing"
task "db:reprocess" do
    $kuiristo.db_reprocess
end




######################################################################
# Import                                                             #
######################################################################

desc "Import ingredients (data+metadata)"
task "db:import:ingredients"  do
    dir = "data/ingredients"

    file = "#{dir}/categories.yaml"
    if File.exist?(file)
        puts "Importing ingredients categories"
        data = YAML.load_file(file)
        DB[:categories_for_ingredients].insert_ignore
            .multi_insert(data)
    end

    file = "#{dir}/shopping.yaml"
    if File.exist?(file)    
        puts "Importing shopping categories"
        data = YAML.load_file(file)
        DB[:categories_for_shopping].insert_ignore
            .multi_insert(data)
    end

    file = "#{dir}/ingredients.yaml"
    if File.exist?(file)    
        puts "Importing ingredients (with categories)"
        data = YAML.load_file(file)
        data.each {|ing|
            categories = ing.delete(:categories)
            DB[:ingredients].insert_ignore.insert(ing)
            categories&.split(',')&.each {|category|
                DB[:ingredient_category].insert_ignore
                    .insert(:ingredient => ing[:id], :category => category)
            }
        }
    end

    Kuiristo::SOURCES.each {|source|
        file = "#{dir}/mapping-#{source::NICKNAME}.yaml"
        if File.exist?(file)
            puts "Importing ingredients mapping for #{source::NICKNAME}"
            data = YAML.load_file(file)
            data.each {|id, ingredient, approximated|
                DB[:ingredient_mapping].insert_ignore
                    .insert(:source       => source::ID,
                            :id           => id,
                            :ingredient   => ingredient,
                            :approximated => approximated)
            }
        end
    }
end



desc "Import recipes (metadata)"
task "db:import:recipes" do
    dir = "data/recipes"

    file = "#{dir}/categories.yaml"
    if File.exist?(file)
        puts "Importing recipes categories"
        data = YAML.load_file(file)
        DB[:categories_for_recipes].insert_ignore
            .multi_insert(data)
    end
end

desc "Import recipes (tagging)"
task "db:import:recipes:tagging" do
    dir = "data/recipes"

    Kuiristo::SOURCES.each {|source|
        file = "#{dir}/tagging-#{source::NICKNAME}.yaml"
        if File.exist?(file)
            puts "Importing recipes category tagging for #{source::NICKNAME}"
            data = YAML.load_file(file)
            data.each {|recipe, categories|
                list = categories.map {|category| { :source   => source::ID,
                                                    :recipe   => recipe,
                                                    :category => category } }
                DB[:recipe_category].insert_ignore
                    .multi_insert(list)
            }
        end
    }
    
end



desc "Import users"
task "db:import:users" do
    dir = "data/users"

    file = "#{dir}/users.yaml"
    if File.exist?(file)
        puts "Importing users list"
        data = YAML.load_file(file)
        DB[:users].insert_ignore.multi_insert(data)
    end
end

desc "Import users (favorites)"
task "db:import:users:favorites" do
    dir = "data/users"

    puts "Importing users favorites"
    DB[:users].select(:id).each {|id:|
        file = "#{dir}/favorites-#{id}.yaml"
        if File.exist?(file)
            puts "Importing user #{id} favorites"
            data = YAML.load_file(file)
            data.map! {|source, recipe| { :user   => id,
                                          :source => source,
                                          :recipe => recipe } }
            DB[:user_favorite_recipe].insert_ignore
                .multi_insert(data)
        end
    }
end


desc "Import ingredients/recipes/users"
task "db:import" => [ "db:import:ingredients",
                      "db:import:recipes",
                      "db:import:users"
                    ] do
end




######################################################################
# Dumping database                                                   #
######################################################################

directory "data/ingredients"
directory "data/recipes"
directory "data/users"

desc "Dump database schema"
task "db:dump:schema" do
    puts "## WARNING: collation is not dumped"
    puts
    puts DB.dump_schema_migration(same_db: true)
end

desc "Dump ingredients (data+metadata)"
task "db:dump:ingredients" => "data/ingredients" do
    dir = "data/ingredients"

    puts "Dumping ingredients categories"
    File.write("#{dir}/categories.yaml", 
               DB[:categories_for_ingredients].order(:id).all
                   .map! {|h| h.compact }
                   .to_yaml)
    
    puts "Dumping shopping categories"
    File.write("#{dir}/shopping.yaml", 
               DB[:categories_for_shopping].order(:id).all
                   .map! {|h| h.compact }
                   .to_yaml)
    
    puts "Dumping ingredients (with categories embedded)"
    File.write("#{dir}/ingredients.yaml", 
               DB[:ingredients]
                   .left_join(:ingredient_category, :ingredient => :id)
                   .select(Sequel.lit('ingredients.*'),
                           Sequel.function(:group_concat, :category)
                               .as(:categories))
                   .group(:id)
                   .order(:id)
                   .all
                   .map! {|h| h.compact }
                   .to_yaml)
    
    Kuiristo::SOURCES.each {|source|
        puts "Dumping ingredients mapping for #{source::NICKNAME}"
        File.open("#{dir}/mapping-#{source::NICKNAME}.yaml", 'w') {|io|
            io << "# Ingredient mapping for #{source::NAME} source\n"
            io << "---\n"
            DB[:ingredient_mapping]
                .select(:id, :ingredient, :approximated).order(:id)
                .where(source: source::ID)
                .each {|id:, ingredient:, approximated:, **o|
                   fmt = approximated ? "%20i, %5i, true" : "%20i, %5i"
                   io << "- [#{fmt}]\n" % [ id, ingredient ] }
        }
    }
end



desc "Dump recipes (metadata)"
task "db:dump:recipes" => "data/recipes" do
    dir = "data/recipes"

    puts "Dumping recipes categories"
    File.write("#{dir}/categories.yaml", 
               DB[:categories_for_recipes].order(:id).all
                   .map! {|h| h.compact }
                   .to_yaml)
    
    Kuiristo::SOURCES.each {|source|
        puts "Dumping recipes category tagging for #{source::NICKNAME}"
        File.open("#{dir}/tagging-#{source::NICKNAME}.yaml", 'w') {|io|
            io << "# Recipe/Categories for #{source::NAME} source\n"
            io << "---\n"
            DB[:recipe_category]
                .select(:recipe,
                        Sequel.function(:group_concat, :category)
                            .as(:categories))
                .group(:recipe)
                .order(:recipe)
                .where(source: source::ID)
                .each {|recipe:, categories:, **|
                        io << "%i: [%s]\n" % [ recipe, categories ] }
        }
    }
end



desc "Dump users"
task "db:dump:users" => "data/users" do
    dir = "data/users"

    puts "Dumping users list"
    File.write("#{dir}/users.yaml", 
               DB[:users].order(:id).all.map! {|h| h.compact }.to_yaml)

    DB[:users].select(:id).each {|id:|
        puts "Dumping user #{id} favorites"
        File.open("#{dir}/favorites-#{id}.yaml", 'w') {|io|
            DB[:user_favorite_recipe]
                .where(:user => id)
                .each {|source:, recipe:, **|
                        io << "- [ %5i, %20i ]\n" % [ source, recipe ] }
        }
    }    
end

desc "Dump all except recipes data"
task "db:dump" => [ "db:dump:recipes",
                    "db:dump:ingredients",
                    "db:dump:users"] do
end

