#desc "Fetch and dump recipe"
#task "debug:recipe:fetch", [:recipe] do |t, args|
#    source, id = $kuiristo.parse_source_recipe(args[:recipe], want: :rawid)
#    puts $kuiristo.fetch(source).recipe(id)
#end


desc "Generate and dump recipe from rawdata"
task "debug:recipe:dump", [:recipe,:fmt] do |t, args|
    source, id = $kuiristo.parse_source_recipe(args[:recipe], want: :rawid)
    puts $kuiristo.recipe(source, id, format: args[:fmt]&.to_sym || :xml)
end


desc "Simple stats"
task "debug:stats" do
    support = []
    support << :phash if defined?(Phash::Image)

    sources = Kuiristo::SOURCES
                  .sort {|a,b| a::NICKNAME <=> b::NICKNAME }
                  .map {|s| "#{s::NICKNAME}[#{s::ID}]" }
    
    puts "Support : #{support.join(', ')}" unless support.empty?
    puts "Sources : #{sources.join(', ')}"
    puts "Recipes : #{$db[:recipes].count}"
    puts "Users   : #{$db[:users  ].count}"
end


desc "Start IRB debugger"
task "debug:irb" do
    require 'irb'

    puts "Global variables:"
    puts "- $config   : configuration"
    puts "- $db = DB  : database sequel handler"
    puts "- $kuiristo : command line processor"
    puts
    Rake::Task["debug:stats"].invoke
    puts
    ARGV.clear
    IRB.start
end


desc "Fetch recipe and dump it"
task "debug:recipe:fetch", [:recipe] do |t, args|
    source, id = $kuiristo.parse_source_recipe(args[:recipe], want: :rawid)

    puts $kuiristo.fetch(source).recipe(id)
end
