# coding: utf-8

require 'dry-types'
require 'dry-struct'
require 'base64'
require 'nokogiri'

module Kuiristo
class Recipe < Dry::Struct

end
end

require_relative 'recipe/types'
require_relative 'recipe/tts'
require_relative 'recipe/ingredient'
require_relative 'recipe/step'
require_relative 'recipe/nutrition'
require_relative 'recipe/info'
require_relative 'recipe/category'
require_relative 'recipe/author'

module Kuiristo
class Recipe < Dry::Struct    
    schema schema.strict
    
    attribute :name,        Types::String
    attribute :source,      Types::String.optional.default(nil)
    attribute :id,          Types::Coercible::Integer
    attribute :origin,      Types::String.optional.default(nil)
    attribute :robot,       Types::Array.of(Types::Robot)
    attribute :portion,     Types::Portion
    attribute :price,       Types::Price.optional
    attribute :difficulty,  Types::Difficulty.optional
    attribute :times,       Types::ClockTimes
    attribute :images,      Types::Strict::Array.of(Types::ImageRef)
    attribute :groups,      Types::Hash
    attribute :nutritions,  Types::Strict::Array.of(Nutrition)
    attribute :infos,       Types::Strict::Array.of(Info)
    attribute :categories,  Types::Strict::Array.of(Category)
    attribute :author,      Author.optional.default(nil)

    P_RECIPE     = "/#{NS_PREFIX}:recipe"
    P_NUTRITION  = "#{P_RECIPE}/#{NS_PREFIX}:nutrition"
    P_TIME       = "#{P_RECIPE}/#{NS_PREFIX}:time"
    P_INFO       = "#{P_RECIPE}/#{NS_PREFIX}:info"
    P_IMAGE      = "#{P_RECIPE}/#{NS_PREFIX}:image"
    P_GROUP      = "#{P_RECIPE}/#{NS_PREFIX}:group"
    P_CATEGORY   = "#{P_RECIPE}/#{NS_PREFIX}:category"
    P_AUTHOR     = "#{P_RECIPE}/#{NS_PREFIX}:author"

    E_STEP       = "#{NS_PREFIX}:step"
    E_IMAGE      = "#{NS_PREFIX}:image"
    E_INGREDIENT = "#{NS_PREFIX}:ingredient"
    
    def xml_builder(xml)
        xml[NS_PREFIX].recipe({
            :source               => source,
            :id                   => id,
            :origin               => origin,
            :name                 => name,
            :robot                => robot.empty? ? nil : robot.join(','),
            :price                => price,
            :difficulty           => difficulty,
            :portion              => portion,
            "xmlns:#{NS_PREFIX}"  => NS_URI,
            'xmlns'               => "http://www.w3.org/1999/xhtml",
        }.compact) {
            xml[NS_PREFIX].time(times.reject {|k,v| v.nil? })
            images.each {|img|
                img = Hash[img.map {|k,v|
                    [ k,
                      case k
                      when :key, :sha256 then Base64.strict_encode64(v)
                      when :sha256       then v.unpack('H*').first
                      else v
                      end ]
                }]
                xml[NS_PREFIX].image(img)
            }
            
            groups.each {|k, g|
                xml[NS_PREFIX].group({:name => k}.reject {|k,v| v.nil? }) {
                    (g[:ingredients]||[]).each {|i| i.xml_builder(xml) }
                    (g[:steps      ]||[]).each {|s| s.xml_builder(xml) }
                }
            }

            nutritions.each {|n| n.xml_builder(xml) }
            infos     .each {|t| t.xml_builder(xml) }
            categories.each {|c| c.xml_builder(xml) }

            author.xml_builder(xml) if author
        }
    end

    def to_xml(encoding: 'UTF-8')
        Nokogiri::XML::Builder.new {|xml|
            xml_builder(xml)
        }.to_xml(:encoding => encoding)
    end


    def self.from_xml(text)
        img_mapper = lambda {|n|
            h = Hash[n.attributes.map {|k,v| [ k.to_sym, v.to_s ] }]

            if h.include?(:sha256)
                case h[:sha256].size
                when 44 then h[:sha256] = Base64.decode64(h[:sha256])
                when 64 then h[:sha256] = [ h[:sha256] ].pack('H*')
                else raise "invalid sha256 encoded string"
                end
            end
            
            h
        }
            
        xml = Nokogiri::XML.parse(text) do |config|
            config.strict.nonet
        end

        

        # Recipe nutrition
        nutritions = xml.xpath(P_NUTRITION).map {|n|
            Nutrition.new(Hash[n.attributes.map {|k,v| [ k.to_sym, v.to_s ] }])
        }

        # Times (Types::ClockTimes.member_types.keys)
        times = xml.xpath(P_TIME).map {|n|
            Hash[n.attributes.map {|k,v|
                     [ k.to_sym,
                       case v = v.to_s
                       when /\.\./
                           range = v.split('..').map {|e| Integer(e) }
                           range.first .. range.last
                       else
                           Integer(v)
                       end
                     ] }]
        }.first || {}

        # Infos
        infos = xml.xpath(P_INFO).map {|n|
            Info.new(:type => n[:type], :text => n.children.to_html.strip)
        }

        # Images
        images = xml.xpath(P_IMAGE).map(&img_mapper)

        # Groups
        groups = Hash[xml.xpath(P_GROUP).map {|n|
            [ n[:name],
              { :steps       =>  n.xpath(E_STEP).map {|n|
                    h = { :text => n.xpath("node()[not(self::#{E_IMAGE})]")
                                       .to_html.strip,
                          :imgs => n.xpath(E_IMAGE).map(&img_mapper)
                        }
                    Step.new(h) },
                :ingredients => n.xpath(E_INGREDIENT).map {|n|
                    h = Hash[n.attributes .map {|k,v| [ k.to_sym, v.to_s ] }]
                    case v = h[:quantity]
                    when /\.\./
                        range = v.split('..').map {|e| BigDecimal(e) }
                        h[:quantity] = range.first .. range.last
                    when nil
                        h[:quantity] = nil
                    else
                        h[:quantity] = BigDecimal(v)
                    end
                    h[:unit] = h[:unit].to_sym
                    Ingredient.new(h) }
              } ]
        }]
        groups.each {|g, h|
            [ :steps, :ingredients ].each {|k|
                h.delete(k) if h[k]&.empty?
            }
        }
        
        # Categories
        categories = xml.xpath(P_CATEGORY).map {|n|
            Category.new(:title => n[:title], :subtitle => n[:subtitle],
                         :from => n[:from], :id => n[:id])
        }

        # Author
        author     = xml.xpath(P_AUTHOR).map {|n|
            Author.new(:url => n[:url], :uid => n[:uid], :name => n.text)
        }.first
        
        # Recipe
        Recipe.new(
            :source     => xml.root[:source],
            :id         => xml.root[:id],
            :origin     => xml.root[:origin],
            :name       => xml.root[:name],
            :robot      => (xml.root[:robot] || '').split(','),
            :price      => xml.root[:price],
            :difficulty => xml.root[:difficulty],
            :portion    => xml.root[:portion],
            :times      => times,
            :nutritions => nutritions,
            :groups     => groups,
            :infos      => infos,
            :images     => images,
            :categories => categories,
            :author     => author,
        )
    end

    def self.image_score(width:, height:, **others)
        wh       = [ width, height ]
        area     = wh.reduce(:*)
        area_min = wh.min * wh.min
        ratio    = wh.min.to_f / wh.max.to_f
        area_min + ((area - area_min) * ratio).to_i
    end
    
    def self.best_image(imgs, score: 352836)
        sorted = imgs.map {|i|
            { score: Recipe.image_score(**i),
              image: i }
        }.sort {|a,b| a[:score] <=> b[:score] }

        (sorted.bsearch {|data| data[:score] >= score } ||
         sorted.last)&.[](:image)
    end
end
end

