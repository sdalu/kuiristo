# coding: utf-8

require_relative '../../fetcher'
require 'pathname'

module Kuiristo
module Source
module Yumi
class Fetcher < Kuiristo::Fetcher
    
    def initialize(cfg={}, logger: nil, **)
        @logger = logger
        @dir    = Pathname(cfg[:dir])
    end
    
    def image(uri)
        File.read("#{@dir}/#{uri}")
    end

    def recipe(key)
        File.read("#{@dir}/#{key}/recipe.xml")
    end

    def index(delay: 0)
        lst = Dir["#{@dir}/*/recipe.xml"].map {|p|
            Pathname(p).relative_path_from(@dir).dirname.to_s }

        @logger&.info "Found #{lst.count} element(s)"

        Hash[lst.map {|key|
                 [ Digest::SHA256.digest(key).unpack('Q').first, key ]
             }]
    end

end
end
end
end
