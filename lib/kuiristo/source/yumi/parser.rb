# coding: utf-8

require_relative '../../parser'

module Kuiristo
module Source
module Yumi
class Parser < Kuiristo::Parser
    def self.extract_images(data, key: nil)
        recp = Recipe.from_xml(data)
        (recp.images +
         recp.groups.flat_map {|k,v| v[:steps].flat_map {|s| s.imgs } })
            .map{|img| if img.include?(:key)
                           [ img[:key], "#{key}/#{img[:src]}" ]
                       elsif img.include?(:src)
                           [ "[#{recp.id}]#{img[:src]}",
                             "#{key}/#{img[:src]}" ]
                       else
                           raise "unspported img attribute"
                       end
        }
    end

    def initialize(data, **opts)
        super(data, **opts)
        @recp = Recipe.from_xml(data)
    end

    def _recipe_origin
        @recp.origin
    end
    
    def _recipe_author
        @recp.author
    end
    
    def _recipe_categories
        @recp.categories.map {|c|
            { :title    => c.title,
              :subtitle => c.subtitle,
              :id       => c.id,
            }
        }
    end
            
    def _recipe_name
        @recp.name
    end        
        
    def _recipe_id
        @recp.id
    end

    def _recipe_robot
        @recp.robot
    end
    
    def _recipe_price
        @recp.price
    end
        
    def _recipe_difficulty
        @recp.difficulty
    end

    def _recipe_portion
        @recp.portion
    end

    def _recipe_times
        @recp.times
    end    
    
    def _recipe_steps
        Hash[@recp.groups.map {|k, v| [k, v[:steps] ] }]
    end

    def _recipe_ingredients
        Hash[@recp.groups.map {|k, v| [k, v[:ingredients] ] }]
    end
    
    def _recipe_additionals
        @recp.infos
    end

    def _recipe_images
        @recp.images.map {|img|
            @dh.imgkey_to_sha256("[#{@recp.id}]#{img[:src]}", source: ID)
        }
        
    end

    def _recipe_nutritions
        @recp.nutritions
    end

end
end
end
end
