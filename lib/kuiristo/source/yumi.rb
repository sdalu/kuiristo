# coding: utf-8

module Kuiristo
module Source
module Yumi
    ID        = 0
    NICKNAME  = :yumi
    NAME      = 'Yumi'
    ICONCLASS = 'icon icon-yumi'
    URL       = 'https://github.com/sdalu/kuiristo-yumi/'
    USAGE     = :'cc-by-sa'
end
end
end

require_relative 'yumi/parser'
require_relative 'yumi/fetcher'


Kuiristo.register_source Kuiristo::Source::Yumi
