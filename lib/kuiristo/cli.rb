# coding: utf-8
require 'sequel'
require 'logger'
require 'json'
require 'msgpack'
require 'zlib'
require 'rmagick'
require 'digest'
require 'set'
require 'yaml'


begin
require 'phash'
require 'phash/image'
rescue LoadError
end
    
require_relative 'data-helper'

module Kuiristo
class CLI
    REGEX_SOURCE_RECIPE    = /^(?<source>\w+)(?<type>[\:\/])(?<id>\d+)$/

    SUPPORTED_IMAGE_FORMAT = [ 'jpeg', 'png' ]

    BLACKLISTED = begin
                      Hash[YAML.load_file('blacklist.yaml')
                               .map {|src,lst| [ src, Set.new(lst) ] }]
                  rescue Errno::ENOENT
                      {}
                  end
        
    VEGLIST     = [ 'semi-vegetarian', 'vegetarian', 'vegan' ]

    def initialize(cfg, db, logger: nil, proxy: nil, parser: nil, **)
        @cfg       = cfg
        @db        = case db
                     when String, Hash     then Sequel.connect(db)
                     when Sequel::Database then db
                     else raise ArguementError, "unsupported db format"
                      end
      # @db.loggers << logger if logger
        @parser    = parser
        @dh        = DataHelper.new(@db)
        @logger    = logger
        @proxy     = proxy
        @fetcher   = {}
    end

    ## Helpers ###########################################################

    # Returns a fetcher instance
    #
    def fetch(source)
        source = Kuiristo.source(source)
        
        @fetcher[source] ||=
            begin
                cfg = @cfg[source::NICKNAME] || {}
                source::Fetcher.new(cfg, logger: @logger, proxy: @proxy)
            end
    end



    def parse_source_recipe(str, want:, allowed: nil)
        raise ArgumentError unless [ :rawid, :id ].include?(want)
        allowed = Array(allowed)

        if allowed.include?(:nil) && str.nil?
            return nil
        end
        
        if m = REGEX_SOURCE_RECIPE.match(str)
            source = Kuiristo.source(m[:source])
            id     = m[:id].to_i

            if    (m[:type] == ':') && (want == :id)
                unless id = @db[:recipes].select(:id)
                                         .first(:source => source::ID,
                                                :rawid  => m[:id].to_i)
                                        &.[](:id)
                    raise "unable to convert rawid to recipe id"
                end
            elsif (m[:type] == '/') && (want == :rawid)
                unless id = @db[:recipes].select(:rawid)
                                         .first(:source => source::ID,
                                                :id     => id)
                                        &.[](:rawid)
                    raise "unable to convert recipe id to rawid"
                end
            end
            [ source::ID, id ]
        elsif allowed.include?(:no_recipe)
            [ Kuiristo.source(str) ]
        else
            raise "unable to parse string"
        end
    end
    

    ## Misc. #############################################################


    def xml(source, rid)
        source = Kuiristo.source(source)
        @logger&.info "XML recipe #{source::NICKNAME}/#{rid}"
        data = @db[:rawdata].first(:source => source::ID, :id => rid)[:zdata]
        data = Zlib::Inflate.inflate(data)
        source::Parser.new(data, dh: @dh, logger: @logger).parse.to_xml
    end

    def recipe(source, rid, format: :xml)
        source = Kuiristo.source(source)
        rstr   = "#{source::NICKNAME}:#{rid}"
        @logger&.info "XML recipe #{rstr}"
        data = @db[:rawdata].first(:source => source::ID, :id => rid)[:zdata]
        data = Zlib::Inflate.inflate(data)
        return data if format == :raw
        recp = source::Parser.new(data, dh: @dh, logger: @logger).parse
        case format
        when :xml then recp.to_xml
        else raise ArgumentError, "unsupported format #{format}"
        end
    end

end
end

require_relative 'cli/fetch'
require_relative 'cli/db'
