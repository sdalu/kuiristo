# coding: utf-8

require 'uri'
require 'faraday'
require 'faraday/cookie_jar'
require 'faraday_middleware'


module Kuiristo
class Fetcher
    class FetcherError < Error
    end

    USER_AGENT   = [
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0)',
        'Gecko/20100101',
        'Firefox/12.0',
    ].join(' ').freeze

    class RequestLogger
        def initialize(app, logger = nil)
            @app    = app
            @logger = logger || Logger.new(STDOUT)
        end
        
        def call(env)
            if @logger && env.method && env.url
                @logger.debug "#{env.method.upcase} #{env.url}"
            end
            @app.call(env)
        end
    end

    def wait(delay)
        Kuiristo.wait(delay) {|wait|
            @logger&.info "Waiting #{wait} seconds before fetching next" }
    end

    def proxy_check!
        if @proxy
            @logger&.info "Using proxy: #{@proxy[:uri]}"
        end
    end

end
end


# Help debugging by registering a request logger
Faraday::Request
    .register_middleware(:logger => Kuiristo::Fetcher::RequestLogger)
