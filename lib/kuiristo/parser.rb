# coding: utf-8

require 'set'

module Kuiristo
class Parser
    using RangeArithmetic

    FRACTIONAL = {
        "\u00BD" => "1/2",
        "\u2153" => "1/3",        "\u2154" => "2/3",
        "\u00BC" => "1/4",        "\u00BE" => "3/4",
        "\u2155" => "1/5",        "\u2156" => "2/5",
        "\u2157" => "3/5",        "\u2158" => "4/5",
        "\u2159" => "1/6",        "\u215A" => "5/6",
        "\u2150" => "1/7",
        "\u215B" => "1/8",        "\u215C" => "3/8",
        "\u215D" => "5/8",        "\u215E" => "7/8",
        "\u2151" => "1/9",
        "\u2152" => "1/10",
    }

        
    NUTRITION_UNITS = {
        /g(?:rammes?)?/                           => :gramme,
        'tasse'                                   => :cup,
        'portion'                                 => :portion,
        'verre'                                   => :glass,
        'bocal'                                   => :jar,
        'morceau/part'                            => :part,
        'morceau'                                 => :part,
        'recette complète'                        => :full,
        'la recette complète'                     => :full,
        'tranche'                                 => :slice,
        'litre'                                   => :litre,
        'bouteille'                               => :bottle,
    }    

    INGREDIENT_UNITS = {
        'verre'                                   => :glass,
        'petit verre'                             => :glass,
        /g(?:rammes?)?/                           => :gramme,
        /tranches?/                               => :slice,
        'bouteille'                               => :bottle,
        'litre'                                   => :litre,
        'cl'                                      => :cl,
        'kg'                                      => :kg,
        'unit'                                    => :unit,
        /unités?/                                 => :unit,
        /pincées?/                                => :pinch,
        /c(?:uillères?|\.)? à soupe rase/         => :sp_level,
        /c\. à s\. rases?/                        => :sp_level,
        /c(?:uillères?|\.) à soupe bombée/        => :sp_heaped,
        /c\.s\. bombées?/                         => :sp_heaped,
        /c\.(?: à )?s\./                          => :sp,
        /c(?:uillères?|\.|) à soupe/              => :sp,
        /c(?:uillères?|\.)? à (?:café|thé) bombée/=> :tsp_heaped,
        /c\.(?: à | )?c\. bombées?/               => :tsp_heaped,
        /c(?:uillères?|\.)? à (?:café|thé) rase/  => :tsp_level,
        /c\.(?: à |à )?c\. rases?/                => :tsp_level,
        /c(?:uillères?|\.|) à (?:café|thé)/       => :tsp,
        /c\.(?: à | |à )?c\./                     => :tsp,
        /carrés?/                                 => :piece,
        'branche'                                 => :branch,
        'morceau'                                 => :piece,
        'petit morceau'                           => :piece_small,
        /brins?/                                  => :sprig,
        'bouquet'                                 => :bouquet,
        'filet'                                   => :fillet,
        /gouttes?/                                => :drop,
        /feuilles?/                               => :leaf,
        'petite feuille'                          => :leaf_small,
        'trait'                                   => :streak,
        /sachets?/                                => :sachet,
        'poignée'                                 => :dose,
        /noix (?=de beurre)/                      => :dose,  # XXX: not sure
        /doses?/                                  => :dose,
        /gobelets? doseur/                        => :dose,
        /quelques?|un peu/                        => :some,
        'paquet'                                  => :pack,
        'conserve'                                => :tin,
        /centimètre|cm/                           => :centimeter,
        'bâton'                                   => :stick,
        /pointe de couteaux?/                     => :kniff_tip,
        'cube'                                    => :cube,
        'boule'                                   => :ball,
        'pot'                                     => :jar,
    }

    
    REGEXP_FLOAT   = /\d+(?:\.\d+)?/
    REGEXP_VALUE   = /  \d+(?:\.\d+|\/\d+)? |  
                        \u00BD        |
                        \u2153        |
                        \u2154        |
                        \u00BC        |
                        \u00BE        |
                        \u2155        |
                        \u2156        |
                        \u2157        |
                        \u2158        |
                        \u2159        |
                        \u215A        |
                        \u2150        |
                        \u215B        |
                        \u215C        |
                        \u215D        |
                        \u215E        |
                        \u2151        |
                        \u2152
                     /x

    class ParserError < Error
        def initialize(msg = nil, data: nil)
            super(msg)
            @data = data
        end
        attr_reader :data
    end

    class ParserStorableError < ParserError
        def initialize(msg = nil, reason: nil, data: nil)
            super(msg, data: data)
            @reason = reason
        end
        attr_reader :reason
    end
        
    PATCHER           = {}
    NAME_TAGGING_LIST = {}
    NAME_SPELLING     = {}


    # Blank normalizer: remove empty or compact space-filed elements
    def self.html_blank_normalizer(f, e) 
        f.css(e).find_all {|n|
            n.children.all?{|c| c.text? && c.content =~ /^\p{Z}*$/u }
        }.each {|n|
            if n.children.any? {|c| c.text? && c.content =~ /\p{Z}/u }
            then n.replace Nokogiri::XML::Text.new(' ', n.document)
            else n.remove
            end
        }
    end

    def self.html_fragment(text)
        Nokogiri::HTML.fragment(text) do |config|
            config.strict.nonet
        end
    end


    
    def self.quantity(str)
        unless m = /^(?<from>#{REGEXP_VALUE})
                     (?:\s* (?:\.\.|-|à|ou) \s*  (?<to>#{REGEXP_VALUE}))?$/x
                       .match(str.strip)
            raise ParserError, "unable to parse quantity string (#{str})"
        end
        
        conv = ->(s) {
            if    s.nil?            then nil
            elsif s.include?('/')   then BigDecimal(s.to_r.to_f, 2)
            elsif v = FRACTIONAL[s] then BigDecimal(v.to_r.to_f, 2)
            else                         BigDecimal(s.to_f, 2)
            end
        }
        
        from = conv.(m[:from])
        to   = conv.(m[:to  ])

        to.nil? ? from : from..to
    end

    def self.unit_lookup(str, table)
        str = (str || 'unit').downcase.strip
        unless unit = table.find {|regex, unit|
                   Regexp === regex ? str =~ /^#{regex}$/x
                                    : str == regex.to_str
               }&.last
            raise ParserError, "unsuported unit (#{str})"
        end
        unit
    end

    def self.ingredient_unit(str)
        self.unit_lookup(str, INGREDIENT_UNITS)
    end


    def self.nutrition_unit(str)
        self.unit_lookup(str, NUTRITION_UNITS)
    end

    
    def initialize(data, logger: nil, dh: nil, ingredient_match: nil)
        @logger           = logger
        @data             = data
        @dh               = dh
        @source           = eval(self.class.name.split('::')[0..-2].join('::'))
        @ingredient_match = ingredient_match
    end


    def parse
        rid    = recipe_id
        @patcher = @source::Parser::PATCHER[rid]
        
        rn  = recipe_name
        @logger&.info "Proccesing recipe: #{rn}"
        
        # Merging ingredients/steps group
        rig = recipe_ingredients
        rsg = recipe_steps

        # Only allow one unnamed group
        crig = rig.compact
        crsg = rsg.compact
        if (crig.size < (rig.size-1)) ||
           (crsg.size < (rsg.size-1))
            raise ParserError, "more than one unnamed group"
        end
        
        if !(rig.keys - rsg.keys).empty? &&  # If not a subset 
           !(rsg.keys - rig.keys).empty? &&  #     from each others
           !(rig.keys == [nil] &&            # Special corner case
             rsg.keys == ["La veille", "Le jour m\u00EAme"])
            
            @logger&.warn {
                diff = "#{rig.keys.inspect} vs #{rsg.keys.inspect}"
                "Ingredients/Steps groups differ: #{diff}"
            }

            # Try to fix common case, where differ from only one key
            # and one of them is nil
            if (rig.keys.size == rsg.keys.size      ) &&
               ((rig.keys     -  rsg.keys).size == 1) &&
               (rig.keys.include?(nil) ^ rsg.keys.include?(nil))
                rik = (rig.keys - rsg.keys).first
                rsk = (rsg.keys - rig.keys).first
                if rik.nil?
                then rig[rsk] = rig.delete(nil)
                else rsg[rik] = rsg.delete(nil)
                end
                @logger&.warn {
                    "Fixed ingredients/steps groups using: \"#{ rik || rsk }\""
                }
            elsif rig.keys == [nil]
                @logger&.warn {
                    "Assuming ingredients hasn't been categorized"
                }
            elsif rsg.keys == [nil]
                @logger&.warn {
                    "Assuming steps hasn't been categorized"
                }
            else
                raise ParserError, "ingredients/steps groups differ"
            end  

        end
        
        recipe_groups = Hash[(rig.keys | rsg.keys).map {|k|
            [k, { :ingredients => rig[k],
                  :steps       => rsg[k] } ] }]

        # Recipe
        Recipe.new(
            :id         => rid,
            :source     => @source::NICKNAME,
            :origin     => recipe_origin,
            :name       => rn,
            :robot      => recipe_robot,
            :price      => recipe_price,
            :difficulty => recipe_difficulty,
            :portion    => recipe_portion,
            :times      => recipe_times,
            :nutritions => recipe_nutritions,
            :groups     => recipe_groups,
            :infos      => recipe_additionals,
            :images     => recipe_images,
            :categories => recipe_categories,
            :author     => recipe_author,
        )
    end

    def recipe_categories
        @logger&.debug "Extracting recipe categories"

        categories  = []

        # Categories derived from recipe name
        set         = Set.new
        name        = _recipe_name.strip
        name.gsub!("\u200B", '')
        { /oe/    => "\u0153",
          /O[eE]/ => "\u0152",
        }.each {|k,v| name.gsub!(k, v) }
        NAME_TAGGING_LIST.each {|key, rules|
            next if key.nil?
            rules.each {|v|
                v = Regexp::escape(v) if String === v
                if m = name.match(v)
                    set << { :title => key, :subtitle => m[1] }
                end
            }
        }
        categories += set.map {|title:, subtitle:|
            @logger&.debug "Injecting category: #{title}/#{subtitle||'(n/a)'}"
            { :title => title.to_s, :subtitle => subtitle, :from => 'autotag' }
        }

        # Categories from recipe
        categories += (_recipe_categories || [])
                          .map {|cat| cat.merge(:from => 'recipe') }
        
        categories.uniq.map {|data| Recipe::Category.new(data) }
    end
        
    def recipe_origin
        @logger&.debug "Extracting recipe origin"
        _recipe_origin
    end

    def recipe_author
        @logger&.debug "Extracting recipe author"

        if ra = _recipe_author
            @logger&.info "Found author: #{ra[:name] || ra[:uid]}"
            Recipe::Author.new(ra)
        end
    end
    
    def recipe_name
        @logger&.debug "Extracting recipe name"

        # Strip name
        name = _recipe_name.strip
        name.gsub!("\u200B", '')
        
        # Correct spelling
        { /oe/    => "\u0153",
          /O[eE]/ => "\u0152",
        }.each {|k,v| name.gsub!(k, v) }
        NAME_SPELLING.each {|k,v|
            case k
            when String then name.gsub!(/(?:\b|\s|^)
                                         #{Regexp.escape(k)}
                                         (?:\b|\s|$)/x, v || '')
            when Regexp then name.gsub!(k,                         v || '')
            else raise ArgumentError
            end
        }

        # Cleaning name
        NAME_TAGGING_LIST.each {|key, rules|
            rules.each {|v| name.sub!(v, '') } }
        name.gsub!(/\(\s*(?:et|,)?\s*\)/, '')
        name.gsub!(/\s+et\s*$/, '')

        
        # Sanitizy name
        name    = name.sub(/^\s*[\-\:]/, '')
                      .sub(/[\-\:]\s*$/, '')
                      .gsub(/\s+/, ' ')
                      .strip

        # Upcase first letter
        name.sub!(/\p{L}/) {|m| m.upcase }

        # Got it
        name
    end        
        
    def recipe_id
        @logger&.debug "Extracting recipe id"

        rid = _recipe_id
        @logger&.info  "Found original recipe id: #{rid}"
        rid
    end

    def recipe_robot
        @logger&.debug "Extracting recipe robot name"

        _recipe_robot
    end
    
    def recipe_price
        @logger&.debug "Extracting recipe price"

        _recipe_price&.downcase
    end
        
    def recipe_difficulty
        @logger&.debug "Extracting recipe difficulty"

        _recipe_difficulty&.downcase
    end

    def recipe_portion
        @logger&.debug "Extracting recipe portions"

        if val = _recipe_portion
            Integer(val)
        end
    end

    def recipe_times
        @logger&.debug "Extracting recipe times"

        # Times
        times = _recipe_times.reject {|k,v| v.nil?}
        
        # Log if some time are not defined
        mtime = Recipe::Types::ClockTimes.member_types.keys
                    .reject {|k| times.key?(k) }
        if !mtime.empty?
            @logger&.debug "no time defined for: #{mtime.join(', ')}"
        end

        # Sanity check times
        times_range = times.dup
        total_range = times_range.delete(:total) { 0 }
        time_range  = times_range.values.reduce {|memo, obj| memo + obj }
                       # XXX: can't use .reduce(:+) due to refinement
        
        if total_range != time_range
            @logger&.warn {
                "Inconsistancy between total times" +
                    " (#{time_range} vs #{total_range})"
            }
        end

        # Commit
        times
    end    
    
    def recipe_steps
        @logger&.debug "Extracting recipe steps"

        # Get group and ensure correct mapping
        grps = _recipe_steps
        grps.each {|k,l| l.map! {|v| Hash===v ? Recipe::Step.new(v) : v } }

        if grps.empty?
            raise ParserStorableError.new("No steps!", reason: 'no-steps')
        end

        # Perform patching if necessary
        if @patcher &&
           @patcher.dig(:group, :steps)&.respond_to?(:call)
        then @patcher.dig(:group, :steps).call(grps)
        else grps
        end
    end

    def recipe_ingredients
        @logger&.debug "Extracting ingredients"

        # Get group and ensure correct mapping
        grps = _recipe_ingredients
        grps.each {|k,l| l.map! {|v| Hash===v ? Recipe::Ingredient.new(v) : v}}

        if grps.empty?
            raise ParserStorableError.new("No ingredients!",
                                          reason: 'no-ingredients')
        end
        
        # Perform patching if necessary
        if @patcher &&
           @patcher.dig(:group, :ingredients)&.respond_to?(:call)
        then @patcher.dig(:group, :ingredients).call(grps)
        else grps
        end
    end
    
    def recipe_additionals
        @logger&.debug "Extracting additional informations"

        _recipe_additionals&.map {|data| Recipe::Info.new(data) }
    end

    def recipe_images
        @logger&.debug "Extracting recipe images"

        _recipe_images
    end

    def recipe_nutritions
        @logger&.debug "Extracting nutrition facts"

        _recipe_nutritions&.map {|data| Recipe::Nutrition.new(data) }
    end

    
    protected

    def lookup_ingredient(id, name)
        log_prefix = "Ingredient #{@source::NICKNAME}/#{id}: "

        # Find ingredident by id
        if ingredient = @dh.ingredient(id, source: @source::ID)
            @logger&.debug "#{log_prefix}found #{ingredient[:id]}"
            return ingredient
        end

        # or try matching by name
        if ingredients = @dh.ingredients_matching(name)
            case ingredients.count
            when 1
                ingredient = ingredients.first
                iid        = ingredient[:id]
                info       = []
                case @ingredient_match
                when :insert
                    @dh.set_ingredient_mapping(@source, id, iid)
                    info << 'inserted'
                end
                info = if info.empty?
                       then ""
                       else " (" + info.join(', ') + ")"
                       end
                @logger&.info "#{log_prefix}match #{iid}#{info}"
                return ingredient
            when 0
                @logger&.error "#{log_prefix}nomatch <#{name}>"
            else
                @logger&.error {
                    matches = ingredients.map {|i| i[:id] }.join(', ')
                    "#{log_prefix}ambiguous match <#{name}> {#{matches}}" }
            end
        else
            @logger&.warn "#{log_prefix}notfound (matching disabled)"
        end

        # Failed
        nil
    end
        
end
end


