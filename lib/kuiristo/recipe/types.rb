# coding: utf-8

module Kuiristo
class Recipe
module Types
    include Dry.Types()
       
    Robot      = Strict::String.enum(*ROBOT_LIST)
    Difficulty = Strict::String.enum('advanced', 'medium', 'easy')
    Price      = Strict::String.enum('low', 'medium', 'high').optional
    Portion    = Coercible::Integer
    ClockTimes = Hash.schema(
        :robot     => Coercible::Integer.optional.default(nil),
        :waiting   => Coercible::Integer.optional.default(nil),
        :active    => Coercible::Integer.optional.default(nil),
        :baking    => (Coercible::Integer | Range).optional.default(nil),
        :total     => Coercible::Integer.optional.default(nil)).strict
    Quantity  = Strict::Decimal.constrained(:gteq => 0, :lteq => 99999)
    
    ImageRef  = Hash.schema(:key    => String).strict |
                Hash.schema(:src    => String).strict |
                Hash.schema(:sha256 => String,
                            :width  => Coercible::Integer,
                            :height => Coercible::Integer,
                            :weight => Coercible::Integer).strict
end
end
end
