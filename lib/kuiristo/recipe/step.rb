# coding: utf-8

module Kuiristo
class Recipe
class Step < Dry::Struct
    schema schema.strict
    
    attribute :text, Types::String
    attribute :imgs, Types::Array.of(Types::ImageRef).default([].freeze)
    
    def xml_builder(xml)
        xml[NS_PREFIX].step {
            xml.parent.add_child(Nokogiri::XML.fragment(text))
            imgs.each {|img|
                img = Hash[img.map {|k,v|
                        [ k,
                          case k
                          when :key, :sha256 then Base64.strict_encode64(v)
                          when :sha256       then v.unpack('H*').first
                          else v
                          end ] }]
                xml[NS_PREFIX].image(img)
            }
        }
    end
    
    def to_html(**o)
        Kuiristo.to_html(text, **o)
    end
end
end
end
