# coding: utf-8

module Kuiristo
class Recipe
class Author < Dry::Struct
    
    schema schema.strict
    
    attribute :url,         Types::String
    attribute :uid,         Types::String
    attribute :name,        Types::String
    
    def xml_builder(xml)
        xml[NS_PREFIX].author({ url: url, uid: uid }.compact) {
            if name && !name.empty?
                xml.parent.add_child(Nokogiri::XML.fragment(name))
            end
        }
    end

    def to_html
        '<a href="%s" data-uid="%s">%s</a>' % [
            url, uid, name || uid
        ]
    end
    
    def to_s
        name || uid
    end

end
end
end
