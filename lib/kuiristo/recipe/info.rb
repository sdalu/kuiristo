# coding: utf-8

module Kuiristo
class Recipe
class Info < Dry::Struct
    TYPES = [ 'background', 'beverage', 'serving_size', 'tip', 'variation' ]

    schema schema.strict
    
    attribute :type,        Types::String.enum(*TYPES)
    attribute :text,        Types::String
    
    def xml_builder(xml)
        xml[NS_PREFIX].info(:type => type) {
            xml.parent.add_child(Nokogiri::XML.fragment(text))
        }
    end

    def to_html(**o)
        Kuiristo.to_html(text, **o)
    end
end
end
end
