# coding: utf-8

module Kuiristo
class Recipe
class TTS  # Time / Temperature / Speed
    class NotValid < Error ; end

    
    KEYS_ORDER    = [ :time, :temp, :reverse, :speed, :repeat ]

    SYMBOLS        = {
        :temp  => { :'lid-closed' => [
                        0, "icon icon-lid-closed",   "couvercle fermé" ],
                    :'varoma'     => [
                        255, nil,                    "Varoma"          ],
                  },
        :speed => { :'stirring'   => [
                        102, "icon icon-stirring",   "mijotage"        ],
                    :'dough-mode' => [
                        101, "icon icon-dough-mode", "pétrissage"      ],
                    :'turbo'      => [
                        100, nil,                    "Turbo"           ],
                  }
    }

    def self.from_hash(h)
        # Reject potential namespace from hash
        h = h.reject {|k,v| k = k.to_s
            k.start_with?('xmlns:') || k == 'xmlns' }
        # Build TTS
        self.new(Hash[h.map {|k,v| [k, self.parse_value(v)] }])
    end

    
    def self.from_string(value, tokenizer: '/')
        self.from_strings(*value.split(tokenizer))
    end

    
    def self.from_strings(*values, tokenizer: nil)
        tts    = {}
        failed = []
            
        values = values.flat_map {|v| v.split(tokenizer) } if tokenizer
        values.each {|s|
            s    = s.sub(/^\p{Z}+/, '').sub(/\p{Z}+$/, '') # Strip on steroid
            k, v = parse_human_string(s, FR_TABLE)
            
            if k === NOT_FOUND      # Not found :(
                failed << s
            elsif tts.include?(k)   # Check for duplicated key
                raise NotValid, "duplicate key in decoding <#{values}>"
            else                    # Got it, add it
                tts[k] = v
            end
        }

        # Was it really a TTS field
        if tts.empty?
            raise NotValid, "no elements found"
        elsif !failed.empty?
            raise NotValid, "unable to decode <#{failed.first}> (#{values})"
        elsif (tts.size == 1) && [ :time ].include?(tts.keys.first)
            raise NotValid, "single key (#{tts.keys.first})"
        end

        # Yeah!
        self.new(tts)
    end

    
    def initialize(tts)
        # Check for unknown keys
        if ! (Hash === tts)
            raise ArgumentError, "expecting a hash"
        end
        if ukey = (tts.keys - KEYS_ORDER).first
            raise ArgumentError, "unexpected key: #{ukey}"
        end
        
        # Check for out of bound value
        check_value!(:time,   tts[:time  ], range: 0..30000)
        check_value!(:temp,   tts[:temp  ], range: 0..200,
                                          symbols: SYMBOLS[:temp ].keys)
        check_value!(:speed,  tts[:speed ], range: 0..10,
                                          symbols: SYMBOLS[:speed].keys)
        check_value!(:repeat, tts[:repeat], range: 1..10)

        # Ok
        @tts = tts
    end


    def to_xml
        "<#{NS_PREFIX}:tts xmlns:#{NS_PREFIX}=\"#{NS_URI}\" " +
            KEYS_ORDER.select {|k| @tts.include?(k) }
                .map    {|k| "#{k}=\"#{@tts[k]}\"" }
                .join(' ') + '/>'
    end
    
    def to_html(elt: 'span', cls: 'kuiristo-settings', **o)
        [ "<#{elt} class=\"#{cls}\">",
          KEYS_ORDER.select {|k| @tts.include?(k) }
               .map    {|k| v = @tts[k]
            case k
            when :time
                case v
                when Numeric
                    min = v.to_i / 60
                    sec = v      % 60
                    txt = []
                    txt << "#{min} min" if !min.zero?
                    txt << "#{sec} sec" if !sec.zero?
                    txt.join(' ')
                when Range
                    min1 = v.first.to_i / 60
                    sec1 = v.first      % 60
                    min2 = v.last.to_i  / 60
                    sec2 = v.last       % 60
                    if    min1.zero? && min2.zero?
                        "#{sec1}-#{sec2} sec"
                    elsif sec1.zero? && sec2.zero?
                        "#{min1}-#{min2} min"
                    else
                        txt1 = []
                        txt1 << "#{min1} min" if !min1.zero?
                        txt1 << "#{sec1} sec" if !sec1.zero?
                        txt2 = []
                        txt2 << "#{min2} min" if !min2.zero?
                        txt2 << "#{sec2} sec" if !sec2.zero?
                        "#{txt1.join(' ')} - #{txt2.join(' ')}"
                    end
                end
            when :temp
                case v
                when Numeric
                    "#{v}\u00B0C"
                when Symbol
                    _, i, t = SYMBOLS[:temp][v]
                    if i.nil?
                    then t
                    else '<i class="%s" title="%s"></i>' % [ i, t ]
                    end
                end
            when :speed
                case v
                when Numeric
                    "<i class=\"icon icon-speed\"></i> #{v}"
                when Range
                    "<i class=\"icon icon-speed\"></i> #{v.min}\u2192#{v.max}"
                when Symbol
                    _, i, t = SYMBOLS[:speed][v]
                    if i.nil?
                    then "<i class=\"icon icon-speed\"></i> #{t}"
                    else '<i class="%s" title="%s"></i>' % [ i, t.capitalize ]
                    end
                end
            when :reverse
                case v
                when true
                    "<i class=\"icon icon-reverse\"" +
                        " title=\"Sens inverse\"></i>"
                end
            when :repeat
                case v
                when Numeric then "#{v} fois"
                when Range   then "#{v.min}-#{v.max} fois"
                end
            end
        }.compact.join(' / '), # \u202F narrow no-break space
         "</#{elt}>" ].join
    end

    private

    NOT_FOUND  = Object.new.freeze

    CONVERTERS = {
        :time   => ->(m1: 0, s1: 0, m2: nil, s2: nil) {
            # Try to fix 30-1 min for 30s - 1min
            if !m2.nil? && (m1 < 60) && (m1 > m2) && !s1&.>(0) && !s2&.>(0)
                m1, s1 = 0, m1 
            end
            v = m1 * 60 + s1
            v = v .. ((m2 || 0) * 60 + (s2 || 0)) if m2 || s2
            v
        }, 
        :temp   => ->(tc: nil, tf: nil) {
            tf ? ((tf - 32) * 5.0 / 9.0).round : tc
        },
        :speed  => ->(s1:, s2: nil) {
            s2 ? (s1..s2) : s1
        },
        :repeat => ->(r1:, r2: nil) {
            r2 ? (r1..r2) : r1
        }
    }
    
    FR_TABLE  = [
        # Time: m1s1 .. m2s2
        [ :time,    %r{\*?(?<s1>\d+(?:[,.]\d+)?)\p{Z}*sec(?:ondes?)?.?}ui  ],
        [ :time,    %r{(?<s1>\d+)\p{Z}*-\p{Z}*(?<s2>\d+)\p{Z}*sec}ui       ],
        [ :time,    %r{(?<m1>\d+)\p{Z}*(?:min(?:utes?)?|mn)
                              (?:\p{Z}*(?<s1>\d+)(?:\p{Z}*sec)?)?}xui      ],
        [ :time,    %r{(?<m1>\d+)\p{Z}*(?:-|\u00E0)\p{Z}*
                        (?<m2>\d+)\p{Z}*min(?:utes?)?}xui                  ],
        [ :time,    %r{(?<s1>\d+)\p{Z}*sec\p{Z}*(?:-|\u00E0)
                                  \p{Z}*(?<m2>\d+)\p{Z}*min}xui            ],
        [ :time,    %r{(?:cuire \p{Z})? 
                       (?:le \p{Z})? temps \p{Z} 
                       (?:de \p{Z} cuisson \p{Z})?
                       indiqué
                       (?:\p{Z} sur \p{Z} le \p{Z} paquet)}xui             ],
        [ :time,    "le temps choisi"                                      ],
        [ :time,    "sans programmer le temps"                             ],
        [ :time,    "Arr\u00EAter quand c'est pr\u00EAt"                   ],

        # Temperature: tc | tf
        [ :temp,    %r{^(?<tc>\d+)\p{Z}*\u00B0(?:\p{Z}*C)?$}ui             ],
        [ :temp,    %r{^varoma$}ui,                              :'varoma' ],
        [ :temp,    "\uE009",                                :'lid-closed' ],

        # Speed: s1 .. s2
        [ :speed,   'stirring',                                :'stirring' ],
        [ :speed,   'mijotage',                                :'stirring' ],
        [ :speed,   %r{(?:vitesse|speed)\p{Z}*\uE002}ui,       :'stirring' ],
        [ :speed,   %r{(?:vitesse|speed)\p{Z}*(?<s1>\d+(?:[,.]\d+)?)
                             (?:\p{Z}*-\p{Z}*(?<s2>\d+(?:[,.]\d+)?))?}xui  ],
        [ :speed,   "\uE001",                                :'dough-mode' ],
        [ :speed,   'dough-mode',                            :'dough-mode' ],
        [ :speed,   /(?:mode|fonction) (?:pétrin|épi)/i,     :'dough-mode' ],
        [ :speed,   %r{turbo}ui,                                  :'turbo' ],

        # Reverse
        [ :reverse, "\uE003",                                         true ],
        [ :reverse, 'reverse',                                        true ],
        [ :reverse, 'sens inverse',                                   true ],
     
        # Repeat: r1 .. r2
        [ :repeat,  %r{          (?<r1>\d+)\p{Z}*
                       (?:-\p{Z}*(?<r2>\d+)\p{Z}*)?fois$}xui               ],
    ]        

    #
    # Possible string values are:
    # - boolean (yes|no|true|false)
    # - integer | float
    # - symbol
    # - range (integer|float)
    def self.parse_value(str)
        return if str.nil? || str.empty?
        a, b = str.split('..')
        a = case a.downcase
            when "yes", "true"  then true
            when "no",  "false" then false
            else (Integer(a) rescue Float(a)) rescue a.to_sym
            end
        if b
        then a .. (Integer(b) rescue Float(b))
        else a
        end
    end

    def self.parse_human_string(str, table, converters: CONVERTERS)
        v = table.each {|key, matcher, action|
            # Match by regular expression
            if Regexp === matcher
                if m = /^#{matcher}$/x.match(str)
                    h = m.named_captures.compact
                    h = Hash[h.map {|k,v| v = v.sub(',', '.')
                              [ k.to_sym, (Integer(v) rescue Float(v)) ] }]
                    converter = converters[key]
                    action    = converter.(h) if !h.empty? && !converter.nil?
                    break [ key, action ]
                end

            # Match by string
            else
                if str == matcher.to_str
                    break [ key, action ]
                end
            end
        }

        v === table ? NOT_FOUND : v
    end


    def check_value!(name, value, range: nil, symbols: nil)
        return nil if value.nil?
        
        if range
            (case value
             when Numeric then [ value ]
             when Range   then [ value.first, value.last ]
             else              []
             end).each {|v|
                if !range.include?(v)
                    raise  NotValid,
                           "#{name} out of boundary [#{range}] (#{value})"
                end
            }
        end
        
        if symbols
            if value.kind_of?(Symbol) && !symbols.include?(value)
                symbols_set = symbols.map{|e| ":#{e}" }.join(',')
                raise "not in symbol set [#{symbols_set}] (#{value})"
            end
        end
        true
    rescue NotValid => e
        raise NotValid, "value <#{v}> for :#{name} is #{e.message}"
    end
    
    
end
end
end
