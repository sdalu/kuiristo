# coding: utf-8

module Kuiristo
class Recipe
class Nutrition < Dry::Struct
    
    attribute  :quantity,    Types::Coercible::Integer
    attribute  :unit,        Types::Strict::String
    attribute? :energy,      Types::Coercible::Float
    attribute? :protein,     Types::Coercible::Float
    attribute? :carb,        Types::Coercible::Float
    attribute? :fat,         Types::Coercible::Float
    attribute? :cholesterol, Types::Coercible::Float
    attribute? :fibre,       Types::Coercible::Float

    def xml_builder(xml)
        attrs = Hash[self.class.schema.keys.map {|k| [k, self.send(k) ] }]
        xml[NS_PREFIX].nutrition(attrs.compact)
    end
end
end
end
