# coding: utf-8

module Kuiristo
class Recipe
class Category < Dry::Struct

    schema schema.strict
    
    attribute :title,         Types::String
    attribute :subtitle,      Types::String.optional
    attribute :id,            Types::Coercible::Integer.optional.default(nil)
    attribute :from,          Types::String
                                  .enum('recipe','autotag','curated')
                                  .optional.default(nil)

    def xml_builder(xml)
        attrs = Hash[self.class.schema.keys.map {|k| [k, self.send(k) ] }]
        xml[NS_PREFIX].category(attrs.compact)
    end
    
    def text
        text  = title
        text += ' (' + subtitle + ')' if subtitle
        text
    end
    
end
end
end
