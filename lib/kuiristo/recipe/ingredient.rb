# coding: utf-8

module Kuiristo
class Recipe
class Ingredient < Dry::Struct
    using ValueDisplay

    SOME  = 'quelques'
    UNITS = [ # A subset of Kuiristo::UNITS
        :glass, :gramme, :slice, :bottle, :cl, :unit, :pinch,
        :sp, :sp_level, :sp_heaped, :tsp, :tsp_heaped, :tsp_level,
        :branch, :piece, :piece_small, :sprig, :bouquet, :fillet,
        :drop, :leaf, :leaf_small, :streak, :sachet,
        :dose, :some, :pack, :tin, :centimeter, :stick, :kniff_tip,
        :cube, :ball, :jar
    ]

    def initialize(*args)
        super(*args)
        @quantity = nil if unit == :some
    end
    
    schema schema.strict
    
    attribute :id,          Types::Coercible::Integer.optional.default(nil)
    attribute :optional,    Types::Params::Bool.optional.default(nil)
    attribute :quantity,    (Types::Coercible::Decimal | Types::Range).optional
    attribute :unit,        Types::Symbol.enum(*UNITS)
    attribute :preparation, Types::String.optional.default(nil)
    attribute :notation,    Types::String

    def xml_builder(xml)
        attrs = Hash[self.class.schema.keys.map {|k| [k, self.send(k) ] }]
        attrs[:quantity] = Kuiristo.value_to(quantity, :string, nil: nil)
        xml[NS_PREFIX].ingredient(attrs.compact)
    end
    
    def to_s
        _quantity, _unit = Kuiristo.denormalize_quantity_unit(quantity, unit)
        unit_s = if _quantity.nil? && ![:some, :unit].include?(_unit)
                 then SOME + ' ' + Kuiristo.unit_name(_unit, 10) || ''
                 else Kuiristo.unit_name(_unit, _quantity) || ''
                 end
        qty_unit_s       = _quantity.to_display;
        qty_unit_s      += ' ' + unit_s if !unit_s.empty?

        if qty_unit_s.empty?
        then notation
        else qty_unit_s + ' ' + notation 
        end
    end

end
end
end
