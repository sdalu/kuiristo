# coding: utf-8

require 'json'

module Kuiristo
class DataHelper
    # From the database table :ingredients
    DB_INGREDIENT_DEFINITION_KEYS = [
        :name, :variant, :part,
        :size, :mix, :maturity, :skin, :salted, :sugared, :fresh,
        :treatment, :preserving, :processing, :shape, :cooking, :packaging ]

    def initialize(db, ingredients_matching: false)
        @db = db
    end
    
    NEUTRAL_SINGULAR = {
        /(?<![aiou])s$/ => '',
        /eaux$/         => 'eau',
        /ée$/           => 'é',
        /ue$/           => 'u',
        /yenne$/        => 'yen',
        /aîche$/        => 'frais',
        /^graine?s?/    => 'grain',
        /^cuite?s?/     => 'cuit',
        /^fraiches?/    => 'frais',
    }

    # For a recipe, retrieve the list of ingredients and their
    # quantities (different quantity unit can be used at the same time)
    #
    def recipe_ingredients_quantities(source, recipe)
        Hash[@db[:recipe_ingredient]
                 .select(:ingredient, :quantities)
                 .where(:source => source, :recipe => recipe)
                 .map {|ingredient:, quantities:|
                        [ ingredient,
                          DataHelper.parseQuantities(quantities)
                        ] }]
    end

    
    def self.parseQuantities(str)
        # Quantities is saved as json hash
        # but we need to deal with range saved as string
        Hash[JSON.parse(str).map {|key, value|
                 [ key.to_sym,
                   case value
                   when String
                       a,b = value.split('..')
                       BigDecimal(a)..BigDecimal(b)
                   else value
                   end ] } ]
    end

    
    # @return 'omnivore', 'semi-vegetarian', 'vegetarian', 'vegan'
    # @return 'ambiguous'  some engridients are not qualified
    # @return 'unknown'    that souldn't happen
    def vegprofil(source, recipe_id)
        iprofils = @db[:recipe_ingredient]
                       .left_join(:ingredients, :id => :ingredient)
                       .where(:source => source, :recipe => recipe_id)
                       .select(:profil)
                       .map(:profil)
        [ nil, 'omnivore', 'semi-vegetarian', 'vegetarian', 'vegan'
        ].find(proc { 'unknown' }) {|k| iprofils.include?(k) } || 'ambiguous'
    end
    
    def imgkey_to_sha256(key, source:)
        @db[:key_image]
            .left_join(:images, :sha256 => :sha256)
            .select(Sequel.qualify(:images, :sha256),
                    :height, :width, :weight)
            .first(Sequel[:key_image][:source] => Kuiristo.source(source)::ID,
                   Sequel[:key_image][:key   ] => Sequel.blob(key))
    end

    
    def ingredient(id, source: nil)
        if i = if source
                   @db[:ingredient_mapping]
                       .select(Sequel.qualify(:ingredients, :id),
                               Sequel.qualify(:ingredients, :singular),
                               Sequel.qualify(:ingredients, :plural),
                               *DB_INGREDIENT_DEFINITION_KEYS)
                       .left_join(:ingredients, :id => :ingredient)
                       .first(:source => Kuiristo.source(source)::ID, 
                              Sequel.qualify(:ingredient_mapping, :id) => id)
               else
                   @db[:ingredients]
                       .select(:id, :singular, :plural,
                               *DB_INGREDIENT_DEFINITION_KEYS)
                       .first(:id => id)
               end
            i.merge(:idname => DB_INGREDIENT_DEFINITION_KEYS
                                   .map {|k| i[k]   }.compact
                                   .map {|n| n.to_s }.join(','))
        end
    end

    def set_ingredient_mapping(source, id, ingredient)
        @db[:ingredient_mapping]
            .insert_ignore
            .insert(:source     => Kuiristo.source(source)::ID,
                    :id         => id,
                    :ingredient => ingredient)
    end
    
    def ingredients_matching(text)
        return nil if text.nil? || ingredients_matching_set.nil?
        
        word_grouping = -> (data) {
            0.upto(data.size-1).flat_map {|i|
                h, t = data[0..i].join(' '), data[i+1..-1]
                if t.empty?
                then [ [ h ] ]
                else word_grouping.(t).map {|r| [ h ] + Array(r) }
                end
            }
        }
        
        # Normalize and mispelling
        text  = Kuiristo.normalize_string_ci(text)
        
        # Split in word
        words = text.split(/\p{Z}/)
        # Deal with <d'>
        words_variations = DataHelper.array_expander(words.map {|w|
                                      w =~ /^d\'/ ? [ w, $' ] : w })
        
        # For each word look if we can add the masculine-singular variant
        # and expand to all the combinations
        variants = []
        words_variations.each {|w|
            variants += DataHelper.array_expander(w.map {|f|
              n = NEUTRAL_SINGULAR.inject(f) {|obj, (k,v)| obj.gsub(k, v) }
              n != f ? [ f, n ] : n
             })
        }
        # Perform word groupings
        combinations = variants.flat_map {|f|
            word_grouping.(f)
        }

        # Cleanup from standalone articles
        #  XXX: we are fuck for "des de jambon"
        combinations.map!{|wg|
            wg.reject {|w|
                [ 'en', 'de', 'des', 'à', 'à la', 'au', 'aux' ].include?(w)
            }
        }

        # Sort to make it easier to search
        combinations.map!{|wg| wg.sort }

        matching = combinations.flat_map {|wg|
            ingredients_matching_set[wg]
        }.compact                           

        matching.map{|id| ingredient(id) }
    end


    private

    def self.array_expander(data)
        h, t = data.first, data[1..-1]
        if t.empty?
        then Array(h).map {|e| [ e ] } 
        else self.array_expander(t).flat_map {|e|
                 Array(h).map {|p| [ p ]  + e }
             }
        end
    end

    private
    
    def ingredients_matching_set
        @ingredients_matching_set ||= begin
            all = @db[:ingredients]
              .select(:id, *DB_INGREDIENT_DEFINITION_KEYS).all.map {|id:, **o|
                l = o.map {|k,v|
                    case k
                    when :size
                        case v
                        when 'small'       then 'petit'
                        when 'medium'      then 'moyen'
                        when 'big'         then 'gros'
                        end
                    when :mix
                        [ 'mélange', 'mixe' ] if v
                    when :maturity
                        case v
                        when 'half-ripe'   then [ 'moitier mûr', 'mi-mûr']
                        when 'ripe'        then 'mûr'
                        when 'well-ripe'   then 'bien mûr'
                        end
                    when :skin
                        case v
                        when true          then 'avec peau'
                        when false         then  [ 'sans peau', 'pelé' ]
                        end
                    when :salted
                        case v
                        when true          then 'salé'
                        when false         then [ 'dessalé', 'sans sel' ]
                        end
                    when :sugared
                        case v
                        when true          then [ 'sucré', 'avec sucre' ]
                        when false         then 'sans sucre'
                        end
                    when :fresh
                        'frais' if v
                    when :treatment
                        case v
                        when 'untreated'   then 'non traité'
                        when 'organic'     then 'bio'
                        end
                    when :preserving
                        v&.split(',')&.flat_map {|v|
                            case v
                            when 'brine'      then 'saumure'
                            when 'salt'       then 'salé'
                            when 'confit'     then 'confit'
                            when 'oil'        then 'à l\'huile'
                            when 'syrup'      then 'au sirop'
                            when 'smoked'     then 'fumé'
                            when 'dried'      then [ 'seché', 'déshydraté' ]
                            when 'vinegar'    then 'au vinaigre'
                            end
                        }
                    when :processing
                        v&.split(',')&.flat_map {|v|
                            case v
                            when 'trimmed'    then 'émondé'
                            when 'shelled'    then 'décortiqué'
                            when 'boned'      then [ 'désarêté', 'désossé']
                            when 'pitted'     then 'dénoyauté'
                            when 'cleaned'    then 'nettoyé'
                            when 'drawn'      then 'vidé'
                            when 'dressed'    then [ 'dressé', 'paré' ]
                            when 'denervated' then 'dénervé'
                            when 'disgorged'  then 'dégorgé'
                            end
                        }
                    when :shape
                        case v
                        when 'whole'       then 'entier'
                        when 'powder'      then 'poudre'
                        when 'puree'       then 'puree'
                        when 'flakes'      then 'flocon'
                        when 'grated'      then [ 'râpé', 'moulu' ]
                        when 'shredded'    then 'râpé'
                        when 'slices'      then [ 'tranche', 'effilé' ]
                        when 'pieces'      then 'morceau'
                        when 'chopped'     then 'haché'
                        when 'ground'      then [ 'moulu', 'haché' ]
                        when 'minced'      then 'émincé'
                        when 'diced'       then 'en dés'
                        when 'chips'       then 'pépite'
                        when 'tournedos'   then 'tournedos'
                        when 'cut'         then 'coupé'
                        end
                    when :cooking
                        case v
                        when 'raw'         then 'cru'
                        when 'pre-cooked'  then [ 'précuit', 'pré-cuit' ]
                        when 'half-cooked' then 'mi-cuit'
                        when 'cooked'      then 'cuit'
                        when 'grilled'     then 'grillé'
                        end
                    when :packaging
                        case v
                        when 'canned'      then 'conserve'
                        when 'frozen'      then [ 'surgelé', 'congelé' ]
                        end
                    end
                }.compact
                
                l += [ :name, :variant, :part ].flat_map {|k|
                    o[k]&.downcase&.split(/\s*,\s*/)
                }.compact
                
                [ id, DataHelper.array_expander(l).map {|a|
                      a.map {|t| Kuiristo.normalize_string_ci(t) }.sort } ]
            }
            
            data = {}
            all.each {|k, vlist| vlist.each {|v| (data[v] ||= []) << k } }
            data

        end
    end

    
end
end
