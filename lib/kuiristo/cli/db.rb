module Kuiristo
class CLI
    using RangeArithmetic

    ## Building database parts ###########################################

    def db_build_image_phash(key)
        if !defined?(Phash::Image)
            @logger&.warn "Phash support not installed"
            return
        end
        
        nick = Kuiristo.hashnick(key)
        
        if img = @db[:images].select(:data).first(:sha256 => Sequel.blob(key))
            file = Tempfile.new('phash')
            file.write(img[:data])
            file.close
            phash = Phash::Image.new(file.path).phash.data
            file.unlink
            @logger&.info "Phash for #{nick}: #{phash.to_s(16)}"
            @db[:images].where(:sha256 => Sequel.blob(key))
                        .update(:phash => phash)
        else
            @logger&.warn "Image #{nick} not found"
        end
    end

    
    def db_build_all_image_phash
        if !defined?(Phash::Image)
            @logger&.warn "Phash support not installed"
            return
        end

        lst = @db[:images].select(:sha256).where(:phash => nil).all
        lst.each.with_index {|row, i| 
            @logger&.info "Building phash (#{i+1}/#{lst.size})"
            db_build_image_phash(row[:sha256])
        }
    end
    
    def db_remove_blacklisted(source = nil)
        source &&= Kuiristo.source(source)

        blacklisted = if source
                      then BLACKLISTED.reject {|src,| src != source::NICKNAME }
                      else BLACKLISTED
                      end
        
        blacklisted.each {|src, list|
            src = Kuiristo.source(src)
            list.each {|rawid|
                @logger&.info "Removing #{src::NICKNAME}/#{rawid}"
                @db[:recipes]
                    .where(:source => src::ID, :rawid => rawid)
                    .delete
            }
        }
    end
            

    
    def db_build_xml_recipe(source, rawid)
        source  = Kuiristo.source(source)
        rstr    = "#{source::NICKNAME}:#{rawid}"
        
        @logger&.info "XML recipe #{rstr}"

        data = @db[:rawdata].first(source: source::ID, id: rawid)[:zdata]
        data = Zlib::Inflate.inflate(data)       
        recp = source::Parser.new(data,
                                  dh: @dh,
                              logger: @logger,
                    ingredient_match: @parser&.dig(:ingredient, :on_match))
                   .parse

        xml   = recp.to_xml
        zxml  = Zlib::Deflate.deflate(xml, 9)
        times = Recipe::Types::ClockTimes
                    .member_types.keys.inject({}) {|h, k|
            h.merge(case t = recp.times[k]
                    when nil     then { }
                    when Range   then { :"time_#{k}" => t.min }
                    when Numeric then { :"time_#{k}" => t     }
                    end)
        }

        if recp.images.empty?
            @logger&.warn "No images found for recipe #{rstr}"
            #raise "No images found for recipe"
        end

        img_list = recp.images.map {|i|
                      [ i[:sha256], i[:width], i[:height] ] }
        meta     = {:images => img_list }
        
        @db[:recipes] ## Use insert/update for constrain
            .on_duplicate_key_update(:id, :name, :robot, :portion,
                                     :price, :difficulty, :zxml, :meta,
                                     *times.keys)
            .insert({ :source       => source::ID,
                      :rawid        => rawid,
                      :id           => recp.id,
                      :name         => recp.name,
                      :robot        => recp.robot.join(','),
                      :portion      => recp.portion,
                      :price        => recp.price,
                      :difficulty   => recp.difficulty,
                      :zxml         => Sequel.blob(zxml),
                      :meta         => meta && Sequel.blob(meta.to_msgpack),
                    }.merge(times))
        @db[:rawdata]
            .where(:source => source::ID, :id => rawid)
            .update(:rejected => nil, :processed => Time.now)

    rescue Parser::ParserStorableError => e
        @db[:rawdata]
            .where(:source => source::ID, :id => rawid)
            .update(:rejected => e.reason.to_s)
        @logger&.warn {
            "Marking recipe #{rstr} as rejected due to error (#{e})" }
    rescue Parser::ParserError => e
        @logger&.warn {
            "Skipped recipe #{rstr} due to error (#{e})" }
        #puts data
        raise
    end
    
    def db_build_xml_recipes(source = nil)
        source &&= Kuiristo.source(source)
        lst = @db[:rawdata].exclude(:zdata => nil)
                  .where { (updated > processed) | {:processed => nil} }
        lst = lst.where(:source => source::ID) if source
        lst = lst.select(:source, :id).all

        lst.each.with_index {|h, i| rsource = h[:source]; rid = h[:id]
            if BLACKLISTED[Kuiristo.source(rsource)::NICKNAME]&.include?(rid)
                @logger&.info "Skipping blacklisted recipe #{rid}"
            else
                @logger&.info "Building recipe (#{i+1}/#{lst.size})"
                db_build_xml_recipe(rsource, rid)
            end
        }
    end


    def db_build_index_ingredient(source = nil)
        source &&= Kuiristo.source(source)
        lst = @db[:recipes]
        lst = lst.where(:source => source::ID) if source
        lst = lst.select(:source, :id).all

        lst.each.with_index {|h, i| rsource = h[:source]; rid = h[:id]
            @logger&.info "Building ingredient index (#{i+1}/#{lst.size})"
            db_build_index_ingredient_recipe(rsource, rid)
        }
    end
    
    def db_build_index_ingredient_recipe(source, id)
        source   = Kuiristo.source(source)
        nickname = source::NICKNAME
        src_id   = { :source => source::ID, :id     => id }
        src_recp = { :source => source::ID, :recipe => id }
        
        @logger&.info "Ingredient index for recipe #{nickname}/#{id}"

        row  = @db[:recipes].select(:zxml).first(src_id)
        txt  = Zlib::Inflate.inflate(row[:zxml])
        recp = Kuiristo::Recipe.from_xml(txt)

        # Get list of ingredients, but omit optional
        mapping = recp.groups.flat_map {|k, ingredients: [], **o|
            ingredients.map {|ing|
                unless ing.optional
                    [ src_recp.merge(:ingredient => ing.id),
                      { ing.unit => ing.quantity } ]
                end
            }
        }.compact
        size = mapping.size

        # Some recipe can use the same ingredients several times
        # and with different units
        mapping = mapping.group_by {|key, qty| key }.map {|key, list|
            quantities = list.reduce({}) {|memo, obj|
                _, values = obj
                memo.merge(values) {|key, oldval, newval|
                    (oldval.nil? && newval.nil?) ? nil : oldval + newval
                }
            }
            # Build our own json string to enhance display
            # of BigDecimal, Float, Integer and Range
            json = [ '{',
                     quantities.map {|key, value|
                         "\"#{key}\":" + Kuiristo.value_to(value, :json)
                     }.join(','),
                     '}' ].join
            key.merge(:quantities => json)
        }
        usize = mapping.size
        
        @logger&.debug {
            str  = "Found #{size} ingredients"
            str += " (unique: #{usize})" if usize != size
            str
        }
        
        # Start with clean recipe mapping and insert
        @db[:recipe_ingredient].where(src_recp).delete
        @db[:recipe_ingredient].insert_ignore.multi_insert(mapping)
    end
    


    #
    # @note ingredient index build by {#db_build_index_ingredient}
    #
    # TODO: add "riz" et "pate"
    def db_autotag_recipe(source, rid)
        source = Kuiristo.source(source)
        tags   = []
        clean  = []
        @logger&.info "Autotagging recipe #{source::NICKNAME}/#{rid}"

        # Veg-profil
        # 
        @autotag_veglist ||= VEGLIST.map {|k|
            if r = @db[:categories_for_recipes].select(:id)
                       .first(:group => 'profil', :key => k)
            then r[:id]
            else raise "database doesn't hold mapping for autotag"
            end
        }

        profil = @dh.vegprofil(source::ID, rid)
        if idx = VEGLIST.index(profil)
            clean += VEGLIST.map         {|key| [ 'profil', key ] }
            tags  += VEGLIST[0..idx].map {|key| [ 'profil', key ] }
        end

        # Category infered from ingredient category
        #   ( Pasta / Rice / Potatoes )
        tags += @db[:recipe_ingredient]
            .distinct
            .select(Sequel[:categories_for_ingredients][:key])
            .left_join(:ingredient_category,
                       Sequel[:ingredient_category][:ingredient] =>
                       Sequel[:recipe_ingredient  ][:ingredient])
            .left_join(:categories_for_ingredients,
                       Sequel[:categories_for_ingredients][:id] =>
                       Sequel[:ingredient_category][:category])
            .where(:source => source::ID, :recipe => rid)
            .map(:key)
            .map {|key|
                case key
                when 'pomme-de-terre' then [ 'preparation', 'potatoes' ]
                when 'riz'            then [ 'preparation', 'rice'     ]
                when 'pates'          then [ 'preparation', 'pasta'    ]
                end
        }.compact
        
        # Embedded categories
        #  (for which we know there is no mismatch)
        row   = @db[:recipes].select(:zxml)
                    .first(:source => source::ID, :id => rid)
        txt   = Zlib::Inflate.inflate(row[:zxml])
        recp  = Kuiristo::Recipe.from_xml(txt)
        tags += recp[:categories].map {|cat|
            case cat.from
            when 'autotag'
                case cat.title
                when 'chef'
                    [ 'profil', 'chefs' ]
                when 'baby'
                    if cat.subtitle =~ /(\d+)/
                        [ 'profil', "baby-#{$1}"  ]
                    end
                when 'special'
                when 'gluten-free'
                    [ 'profil', 'gluten-free' ]
                end
            end
        }.compact

        # Cleaning tags
        clean.each {|grp, key|
            @logger&.debug "Removing tag: #{grp}/#{key}"
            @db[:recipe_category]
                .where(:category =>
                       @db[:categories_for_recipes]
                           .select(:id)
                           .where(:group => grp, :key => key))
                .where(:source   => source::ID, :recipe => rid)
                .delete
        }

        # Commit new tags
        tags.each {|grp, key|
            if id = @db[:categories_for_recipes]
                        .select(:id)
                        .first(:group => grp, :key => key)&.[](:id)
                @logger&.debug "Tagging with: #{grp}/#{key}"

                @db[:recipe_category]
                    .insert_ignore
                    .insert(:source   => source::ID, :recipe => rid,
                            :category => id)
            end
        }
    end

    def db_autotag_recipes(source = nil)
        source &&= Kuiristo.source(source)
        lst = @db[:recipes].select(:id, :source)
        lst = lst.where(:source => source::ID) if source
        
        lst.each {|id:, source:, **o|
            db_autotag_recipe(source, id)
        }
    end

    

    ## Database management ###############################################

    def db_remove_orphaned_images
        @logger&.info "Retrieving list of images in database"
        db_imgs  = @db[:images].select(:sha256).map(:sha256)
        @logger&.debug "Found #{db_imgs.size} images in database"
        
        @logger&.info "Retrieving list of images from recipes"
        rcp_imgs = @db[:recipes].select(:zxml).flat_map {| zxml: |
            xml  = Zlib::Inflate.inflate(zxml)
            recp = Kuiristo::Recipe.from_xml(xml)
            
            (recp.images  + 
             recp.groups.values.flat_map {| steps: [], ** |
                 steps.flat_map {|step| step.imgs }
             }).map {|img| img[:sha256] }
        }
        @logger&.debug "Found #{rcp_imgs.size} images in recipe definitions"

        orphaned = db_imgs - rcp_imgs
        if orphaned.size.zero?
        then @logger&.info "No orphaned image found"
        else @logger&.info "Found #{orphaned.size} orphaned images"        
        end
    end
    
    def db_purge
        #@db[:recipe           ].delete
        #@db[:category_food    ].delete
        #@db[:category_shopping].delete
    end

    def db_reprocess(source = nil)
        source &&= Kuiristo.source(source)
        
        set = @db[:rawdata]
        set = set.where(:source => source::ID) if source
        set.update(:processed => nil)
    end

end
end
