module Kuiristo
class CLI

    ## Fetching ##########################################################
        
    # Fetch whole index for recipe source and insert the new
    # recipes id/key into the database
    #
    # @param source [Symbol, Integer, Module]
    # @param delay [Range, Integer] wait time between requests
    #
    def fetch_index(source, delay: 2..5)
        source = Kuiristo.source(source)
        
        fetch(source).index(delay: delay).each {|id, key|
            @db[:rawdata].insert_ignore
                .insert(:source => source::ID, :id => id, :key => key)
        }
    end

    
    # Fetch the recipe "rawdata" and store it into the database
    # @note `source` and `rid` should have already been retrieved
    #       by {#fetch_index}
    #
    # @param source [Symbol, Integer, Module]
    # @param rid [Integer] raw id
    #
    def fetch_recipe(source, rid)
        source = Kuiristo.source(source)
        rstr   = "#{source::NICKNAME}:#{rid}"
        
        @logger&.info "Fetching recipe: #{rstr}"

        # Check if a key is defined
        row = @db[:rawdata].first(:source => source::ID, :id => rid)
        if row.nil?
            @logger&.warn "Recipe #{rstr} not found"
            return
        end
        key = row[:key] || rid
        
        # Fetching
        tstart = Time.now
        data   = fetch(source).recipe(key)
        ftime  = (Time.now - tstart).to_i
        zdata  = Zlib::Deflate.deflate(data, 9)
        
        # Compress and save
        @db[:rawdata]
            .on_duplicate_key_update(:zdata, :updated)
            .insert(:source  => source::ID, :id => rid,
                    :zdata   => Sequel.blob(zdata),
                    :updated => Sequel.function(:now)
                   )

        # Info
        @logger&.debug "Recipe #{rstr} fetched (time: #{ftime})"
    end

    
    # Fetch the recipe dependencies (pictures, ...)
    # @note The recipe should have already been fetched ({#fetch_recipe})
    #
    # @param source [Symbol, Integer, Module]
    # @param rid [Integer] raw id
    #
    def fetch_recipe_dependencies(source, rid)
        source   = Kuiristo.source(source)
        rstr    = "#{source::NICKNAME}:#{rid}"
        
        @logger&.info "Looking for dependencies in recipe #{rstr}"

        row  = @db[:rawdata].first(source: source::ID, id: rid)
        data = Zlib::Inflate.inflate(row[:zdata])
        imgs = source::Parser.extract_images(data, key: row[:key])

        @logger&.info "Found #{imgs.size} image definition(s)"
        
        # Same image can have different keys :(
        imgs.each {|key, uri, **o|
            # Key nickname
            nick = Kuiristo.hashnick(key)

            # Sanity check 
            if key.size >= 256
                raise DBError, "key_image:key.size >= 256 (#{key.size})"
            end

            # Fetch if not present
            if @db[:key_image].select(:sha256)
                   .first(source: source::ID, key: Sequel.blob(key))
                @logger&.debug "Image [#{nick}] already in DB"
            else
                @logger&.info "Fetching image [#{nick}]"

                tstart = Time.now
                data   = fetch(source).image(uri)
                ftime  = (Time.now - tstart).to_i
                sha256 = Digest::SHA256.digest(data)
                img    = Magick::Image.from_blob(data).first
                fmt    = img.format.downcase
                height = img.columns
                width  = img.rows
                if !SUPPORTED_IMAGE_FORMAT.include?(fmt)
                    raise "unsupported image format (#{fmt})"
                end 
                
                @logger&.debug "Fetched #{fmt.upcase} image (size: #{width}x#{height}, time: #{ftime} sec)"

                @db[:images]
                    .on_duplicate_key_update(:weight, :format, :width, :height)
                    .insert(:sha256 => Sequel.blob(sha256),
                            :data   => Sequel.blob(data),
                            :weight => data.size,
                            :format => fmt.downcase,
                            :width  => width,
                            :height => height,
                            :source => source::ID,
                           )
                @db[:key_image]
                    .on_duplicate_key_update(:sha256)
                    .insert(:source => source::ID,
                            :key    => Sequel.blob(key   ),
                            :sha256 => Sequel.blob(sha256))
            end
        }

        @logger&.debug "Marking dependencies for recipe #{rstr} as fetched"
        @db[:rawdata]
            .where(:source => source::ID, :id => rid)
            .update(:dependencies => Time.now)
    end


    # Fetch recipes and insert them into the database
    # @note recipes must have been previously identified by {#fetch_index}
    #
    # @param source [nil, Symbol, Integer, Module]  restrict to `source`
    # @param limit [Integer]  restrict to the first `limit`-elements
    # @param time  [Time]     if set process all recipes older that `time`
    #                         otherwise all new recipes
    # @param delay [Range, Integer] wait time between requests
    #
    def fetch_recipes(source = nil,
                      limit: nil, time: nil, delay: 3..20)
        source &&= Kuiristo.source(source)

        # Fetching recipes parameters
        @logger&.info {
            info = []
            info << "source: #{source::NICKNAME}"      if source
            info << "limit: #{limit || 'none'}"
            info << "older: #{time.strftime('%F %T')}" if time
            "Fetching recipes (#{info.join(', ')})"
        }
        
        lst = @db[:rawdata].select(:source, :id)
        lst = lst.where(:source => source::ID) if source
        lst = lst.limit(limit)                 if limit
        lst = if time
              then lst.exclude { updated > time } 
              else lst.where(:zdata => nil)
              end

        @logger&.info "Found #{lst.count} unfetched recipe(s)"
        lst.each {|source:, id:|
            nickname = Kuiristo.source(source)::NICKNAME
            if @cfg.dig(nickname, :wait)
                Kuiristo.wait(delay) {|wait|
                    @logger&.debug "Waiting #{wait} seconds before querying recipe"
                }
            end
            fetch_recipe(source, id)
        }
    end

    
    # Fetch dependencies and insert them into the database
    # @note recipes must have been previously retrieved by {#fetch_recipes}
    #
    # @param source [nil, Symbol, Integer, Module]  restrict to `source`
    #
    def fetch_recipes_dependencies(source = nil)
        source &&= Kuiristo.source(source)

        @logger&.info {
            nickname = source.nil? ? '-all-' : source::NICKNAME
            "Fetching recipes dependencies for <#{nickname}>"
        }

        lst = @db[:rawdata].exclude(:zdata => nil)
                  .where { (updated > dependencies) | {:dependencies => nil} }
        lst = lst.where(:source => source::ID) if source
        lst = lst.select(:source, :id).all

        @logger&.info "Found #{lst.size} recipe(s) with unfetched dependencies"

        lst.each.with_index {|h, i| rsource=h[:source]; rid=h[:id]
            @logger&.debug {
                nickname = Kuiristo.source(rsource)::NICKNAME
                "Fetching dependencies for #{nickname} recipe #{rid} (#{i+1}/#{lst.size})"
            }
            fetch_recipe_dependencies(rsource, rid)
        }
    end

end
end
