# coding: utf-8

require 'sinatra'
require 'erb'
require 'compass'
require 'json'
require 'msgpack'
require 'date'
require 'mail'
require 'sequel'


require 'kuiristo'
require 'kuiristo/config'  
require 'kuiristo/recipe'
$config.dig(:sources)&.each_key {|source|
require "kuiristo/source/#{source}"
}
require 'kuiristo/core_extensions'
require 'kuiristo/data-helper'



if $config.dig(:smtp, :address)
    Mail.defaults do
        delivery_method :smtp, {
            :address              => $config.dig(:smtp, :address),
            :port                 => $config.dig(:smtp, :port   ),
            :openssl_verify_mode  => OpenSSL::SSL::VERIFY_NONE,
            :enable_starttls_auto => true,
        }.compact
    end
end



class KuiristoWeb < Sinatra::Base
    NAME       = $config.dig(:branding, :name) || 'Kuiristo'
    
    DB         = Sequel.connect({ :encoding  => 'utf8',
                                  :adapter   => 'mysql2'
                                }.merge($config.dig(:db)))
    DH         = Kuiristo::DataHelper.new(DB)
    
    VEGGROUP   = 'profil'
    VEGPROFILS =  { nil               => 'n/a',
	            'omnivore'        => 'omnivore',
	            'semi-vegetarian' => 'semi-végétarien',
	            'vegetarian'      => 'végétarien',
    	            'vegan'           => 'végan',
                  }    
    VEGLIST     = [ 'semi-vegetarian', 'vegetarian', 'vegan' ]
    VEGTAGS     = VEGLIST.map {|key|
                      DB[:categories_for_recipes].select(:id)
                          .first(:group => VEGGROUP, :key => key)&.[](:id)
                  }.compact

    
    CATEGORIES = [
        [ :type,        'Type de plat','fas fa-utensils',         ],
        [ :preparation, 'Préparation', 'fas fa-chart-pie',        ],
        [ :profil,      'Profil',      'far fa-address-card',     ],
        [ :season,      'Saison',      'far fa-calendar-alt',     ],
        [ :origin,      'Origine',     'fas fa-globe',            ],
        [ :source,      'Source',      'fas fa-book',
          Kuiristo::SOURCES.sort {|a,b| a::ID <=> b::ID }.map {|s|
              [ s::NICKNAME.to_s, s::NAME, s::ICONCLASS ||
                                           'icon icon-difficulty' ] }  ]
    ].map {|type, name, iconclass, entries|
        if entries.nil? || entries.empty?
            entries = DB[:categories_for_recipes]
                          .select(:id, :name, :icon)
                          .where(:group => type.to_s)
                          .order(:id)
                          .map {|h| [ h[:id], h[:name], h[:icon] ] }
        end
        unless entries.nil? || entries.empty?
            [ type, name, iconclass, entries ]
        end
    }.compact

    
    TIME_LADDER = {
        :total  => $config.dig(:'time-ladder', :total ) ||
                   [ 10, 20, 30, 45, 60, 75, 90, 120, 180, 360, 720, 1440 ],
        :active => $config.dig(:'time-ladder', :active) ||
                   [ 5, 10, 15, 20, 30, 45, 60 ],
    }
    
    
    NUTRITION_UNITS = {
        :gramme  => [ 'gramme',           'grammes'          ],
        :cup     => [ 'tasse',            'tasses'           ],
        :portion => [ 'portion',          'portions'         ],
        :glass   => [ 'verre',            'verres'           ],
        :jar     => [ 'bocal',            'bocaux'           ],
        :part    => [ 'morceau / part',   'morceaux / parts' ],
        :full    => [ 'recette complète'                     ],
        :slice   => [ 'tranche',          'tranches'         ],
        :litre   => [ 'litre',            'litres'           ],
        :bottle  => [ 'bouteille',        'bouteilles'       ],
    }
    

#    enable :sessions
    
    configure do
        set :sass, Compass.sass_engine_options
        set :scss, Compass.sass_engine_options
    end

    configure do
        set :extlink, { :rel => 'external noreferrer noopener nofollow' }
        set :extlink, false
    end
    
    helpers do
        def is_crawler?
            ($config.dig(:web, :crawler) || [ 'Wget', 'curl' ])
                .include?(request.user_agent.split('/').first)
        end


        
        def ruser
            session['user']
        end

        def is_authenticated!
            return if is_authenticated?
            raise "authenticated user required"
        end

        def is_authenticated?
            !session['user'].nil?
        end


        
        def h(text)
            Rack::Utils.escape_html(text)
        end

        # Sinatra define method "uri", and alias it to "url", "to"
        # Override "to" methode to change the default behaviour
        def to(addr = nil, absolute = false, add_script_name = true)
            uri(addr, absolute, add_script_name)
        end        


        
        def title(title)
            @title = title
        end


        
        def img_relpath(name)
            if Hash === name
                name = name[:sha256].unpack('H*').first
            end
            "#{name[0,2]}/#{name[2,2]}/#{name}"
        end

        def recipe_relpath(source, id)
            "#{source}/#{id%255}/#{id}"
        end


        
        def time_ladder(time, ladder)
            ladder = TIME_LADDER[ladder] if Symbol === ladder
            ladder.map {|t| t * 60 }.bsearch_index {|x| x >= time } ||
                ladder.size
        end
            
        def str_recipe_time(sec, *opts)
            if opts.include?(:nosec)
                sec = (sec / 60.0).ceil * 60 
            end

            min  = sec / 60
            sec  = sec % 60
            hour = min / 60
            min  = min % 60

            # \u202f%02  narrow non breakable
            
            fmt = if hour.zero?
                      if    min.zero? then "%<sec>ds"
                      elsif sec.zero? then "%<min>dm"
                      else                 "%<min>dm%<sec>d"
                      end
                  else
                      if min.zero?    then "%<hour>dh"
                      else                 "%<hour>dh%02<min>d"
                      end
                  end
            fmt % { :hour => hour, :min => min, :sec => sec }
        end

        def html_transform(text)
            return text if settings.extlink === true
            
            f = Nokogiri::HTML.fragment(text) do |config|
                config.nonet # .strict
            end
	    f.css('a').each {|a|
                case settings.extlink
                when true   # keep link as is
                when false  # replace link by span element with data-url
                    a.name = 'span'
                    a['data-url'] = a['href']
                    a.delete('href')
                when nil    # remove link, keep content
                    a.children.reverse.each {|c| a.next = c }
                    a.remove
                when Hash   # add/replace attributs in link
                    settings.extlink.each {|k,v| a[k] = v }
                else
                    raise "unknown extlink setting"
                end
	    }
	    f.to_html
        end
    end

    







    ######################################################################
   

    not_found do
        erb :not_found
    end
end


require_relative 'app/index'                # Recipe index
require_relative 'app/recipe'               # Recipe description
require_relative 'app/picture'              # Pictures
require_relative 'app/json-resources'       # JSON used for searching

require_relative 'app/manifest'             # Progressive web app

require_relative 'app/shopping-cart'        # Shopping cart generation

require_relative 'app/login'                # Login management
require_relative 'app/starring'             # Allow recipes' starring by users

# Editing
if $config.dig(:web, :editing)
require_relative 'app/edit/recipe-category' # Tagging of recipe with categories
require_relative 'app/edit/ingredient'      # Edit ingredient
end
