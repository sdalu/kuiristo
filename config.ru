# -*- ruby -*-

# Ensure loading path
$LOAD_PATH << "#{__dir__}/lib"


# Load kuiristo configuration
require 'kuiristo/config'


# SASS/Compass configuration
#  and integration in Rack for automatic generation
require 'compass'
require 'sass/plugin/rack'

Compass.configuration do |config|
    config.project_path = __dir__
    config.css_dir      = 'public/style'
    config.sass_dir     = 'public/style'
    config.images_dir   = 'assets/images'
    config.additional_import_paths = [ 'assets/sass' ]
    config.environment  = :production
    config.output_style = config.environment == :production ? :compressed
                                                            : :expanded
    config.sass_options = { :always_update => true }
end
Compass.configure_sass_plugin!

use Sass::Plugin::Rack


# Parsing of body requests
#  (default parser for application/json, if json parser available)
require 'json'
require 'rack/parser'
use Rack::Parser


# Allow compression
use Rack::Deflater


# Manage session
require 'securerandom'
use Rack::Session::Cookie, {
        :key          => 'kuiristo.session',
        :path         => $config.dig(:web, :path) || '/kuiristo',
        :expire_after => 2592000,
        :secret       => SecureRandom.hex(10)
    }.merge($config.dig(:web, :session))


# Flash notifications
require 'rack/flash'
use Rack::Flash, :accessorize => [:notice, :error]


#
# Do it
#
require_relative 'app'

map ($config.dig(:web, :path) || '/kuiristo') do
    run KuiristoWeb
end
