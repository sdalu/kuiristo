class KuiristoWeb

    #
    # Recipe starred
    #

    get '/recipe/starred' do
        content_type :json

        DB[:user_favorite_recipe]
            .select(:source, :recipe)
            .where(:user => ruser)
            .all.to_json
    end
    
    get '/recipe/:source/:x/:id/starred' do |source,x,id|
        content_type :json

        is_authenticated!
        
        src = Kuiristo.source(source)
        (DB[:user_favorite_recipe]
             .where(:user => ruser, :source => src::ID, :recipe => id)
             .count > 0).to_json
    end
    
    post '/recipe/:source/:x/:id/starred' do |source,x,id|
        is_authenticated!

        src = Kuiristo.source(source)
        key = { :user => ruser, :source => src::ID, :recipe => id }
        tbl = DB[:user_favorite_recipe]
        case params[:op]
        when '+' then tbl.insert_ignore.insert(key)
        when '-' then tbl.where(key).delete
        else raise "unsupported operation"
        end
        true
    end

    #
    # User
    #
    get '/user/:id/starred' do |id|
        content_type :json

        if id == '^_^'
            id = ruser
        end
        
        DB[:user_favorite_recipe]
            .select(:source, :recipe)
            .where(:user => id)
            .all.to_json
    end
    
end
