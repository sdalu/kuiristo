require 'set'

class KuiristoWeb
    using Kuiristo::ValueDisplay
    using Kuiristo::RangeArithmetic
    using Kuiristo::StringFormatter


    #
    # Shopping cart
    #

    get '/shopping/list.:format' do |format|
        # Retrieve recipe list as content-type json (ajax request)
        # (parsed  by Rack::Parser) or form encoded (form submit),
        # and parse it to source/recipe/count
        #
        #data = env['rack.parser.result'] || JSON.parse(params['data'])
        #data.map! {|source_recipe, count|
        #    source, recipe = source_recipe.split('/')
        #    [ Kuiristo.source(source)::ID, recipe.to_i, count ]
        #}

        shplist = params['for']&.split('|')
                      &.map {|elt| source, recipe, count = elt.split('-')
                          [ Kuiristo.source(source)::ID, recipe.to_i,
                            count.to_i ] }

        checked = Set.new(params['checked']&.split('|')
                              &.map {|e| Integer(e) rescue nil }
                              &.compact
                         )
        
        ignored = Set.new(params['ignored']&.split('|')
                              &.map {|e| Integer(e) rescue nil }
                              &.compact
                         )

        # Build recipe summary
        recipes = shplist.map {|source, recipe, count|
            DB[:recipes]
                .select(:source, :id, :name)
                .first(:source => source, :id => recipe)
                .merge(:count => count)
        }.sort {|a, b| a[:name] <=> b[:name] }
        
        # Merge recipes ingredients quantities together
        # Using RangeArithmetic
        qtys = {}
        shplist.each {|source, recipe, count|
            DH.recipe_ingredients_quantities(source, recipe)
              .each {|ingredient, quantities|
                  # Adjust ingredient quantities
                  quantities.transform_values! {|value|
                      value.nil? ? nil : (value * count)
                  } 
                  # Merge with previous recipes
                  (qtys[ingredient] ||= {})
                      .merge!(quantities) {|_, old, new|
                          (old.nil? && new.nil?) ? nil : (old + new) }
            }
        }
        # Order units
        qtys.transform_values! {|quantities|
            Hash[Kuiristo::UNITS.map {|key, _| [ key, quantities[key] ] }]
                .compact
        }

        # If using compact form
        if ignored
            ignored.each {|id| qtys.delete(id) }
        end
        
        # Find ingredients fullname (using singular form)
        # and order by shopping_id, generic name and ingredient_id
        ingredients = DB[:ingredients]
            .left_join(:categories_for_shopping,
                       Sequel[:categories_for_shopping][:id] =>
                       Sequel[:ingredients][:shopping])
            .select(Sequel[:ingredients][:id],
                    Sequel.function(:coalesce,
                                    :singular, :plural).as(:fullname),
                    Sequel[:ingredients][:name],
                    Sequel[:ingredients][:packaging],
                    Sequel[:categories_for_shopping][:name].as(:shopping))
            .where(Sequel[:ingredients][:id] => qtys.keys)
            .order(Sequel.function(:coalesce,
                                   Sequel[:ingredients][:shopping], 255),
                   Sequel[:ingredients][:name],
                   Sequel[:ingredients][:id])
            .all

        # Join with quantities and make it more human readable by
        #  - converting unit to name (with plural if necessary)
        #  - converting quantity using fraction if possibble (1/2, 1/4, 3/4)
        #  - using special case for SI unit (g)
        ingredients.map! {| data |
            id         = data[:id]
            quantities = qtys[id].map {|unit, qty|
                qty, unit = Kuiristo.denormalize_quantity_unit(qty, unit)
                unit_s    = Kuiristo.unit_name(unit, qty)
                qty_s     = qty.to_display
                [ unit_s, qty_s ]
            }
            data.merge(:quantities => quantities)
        }

        # Perform grouping by shopping category, next by generic name
        # Only keep fullname and quantities as array
        ingredients = ingredients
            .group_by {|shopping:, **| shopping }
            .transform_values! {|shp_grp|
                shp_grp
                    .group_by { |name:, ** | name}
                    .transform_values! {|gn_grp|
                        gn_grp.map { |fullname:, quantities:,
                                      packaging:, id:, **|
                        [ fullname, quantities, :packaging => packaging,
                                                :id        => id          ] }
               }
        }

        locals = { :recipes     => recipes,
                   :ingredients => ingredients,
                   :checked     => checked }

        case format
        when 'txt'
            # List as pure text
            content_type :text
            erb :'shopping-list-txt', :layout => false, :locals => locals
            
        when 'html'
            # List as HTML page
            title "Liste des courses"
            erb :'shopping-list', :locals => locals

        when 'json'
            # List as JSON data (doesn't include recipe summary)
            content_type :json
            ingredients.to_json

        when 'mail'
            if ruser.nil?
                halt "Need to be authenticated for email access"
            end
            
            if sender = $config.dig(:mail, :from)
                # Email list to user
                user = DB[:users].first(:id => ruser)
                html = erb(:'shopping-list',
                           :layout => :'mail-layout', :locals => locals)
                text = erb(:'shopping-list-txt',
                           :layout => false,          :locals => locals)
            
                mail = Mail.deliver do
                    to      user[:email]
                    from    sender
                    subject "Liste des courses (#{Date.today})"
                    
                    text_part do
                        body text
                    end
                    
                    html_part do
                        content_type 'text/html; charset=UTF-8'
                        body html
                    end
                end

                # As a courtesie, display it also as HTML page
                flash[:notice] = "Cette liste vous a egalement ete envoyee par e-mail a l'adresse <tt>#{user[:email]}</tt>"
            else
                flash[:notice] = "L'envoi par mail a ete desactive"
            end

            
            title "Liste des courses"
            erb :'shopping-list', :locals => locals

        else
            halt "Sending email has been disabled"
        end
    end

end
