class KuiristoWeb

    #
    # Picture
    #
    
    head '/picture/:x/:y/:id' do |x,y,id|
        id  = [id].pack('H*')
        img = DB[:images].select(:sha256, :format, :weight, :inserted)
                         .first(:sha256 => Sequel.blob(id))        
        not_found if img.nil?  # Ensure image is found!
        fmt = img[:format] || 'jpeg'

        content_type  "image/#{fmt}"
        headers['Content-Length'] = img[:weight].to_s
        last_modified img[:inserted]
        cache_control :public
        etag          img[:sha256].unpack('H*').first
    end

    get '/picture/:x/:y/:id' do |x,y,id|
        id  = [id].pack('H*')
        img = DB[:images].select(:sha256, :format, :data, :inserted)
                         .first(:sha256 => Sequel.blob(id))
        not_found if img.nil?  # Ensure image is found!
        fmt = img[:format] || 'jpeg'

        content_type  "image/#{fmt}"
        last_modified img[:inserted]
        cache_control :public
        etag          img[:sha256].unpack('H*').first

        img[:data]
    end

end
