class KuiristoWeb

    #
    # Recipe
    #
    
    head '/recipe/:source/:x/:id.raw' do |source,x,id|
        src = Kuiristo.source(source)

        # Find recipe and get rawid
        row = DB[:recipes].select(:rawid)
                          .first(:source => src::ID, :id => id.to_i)
        not_found if row.nil?  # Ensure data is found!

        # Get raw data
        row = DB[:rawdata].select(:updated)
                  .first(:source => src::ID, :id => row[:rawid])

        last_modified row[:updated]
    end
    
    get '/recipe/:source/:x/:id.raw' do |source,x,id|
        src = Kuiristo.source(source)

        # Find recipe and get rawid
        row = DB[:recipes].select(:rawid)
                          .first(:source => src::ID, :id => id.to_i)
        not_found if row.nil?  # Ensure data is found!

        # Get raw data
        row = DB[:rawdata].select(:zdata, :updated)
                          .first(:source => src::ID, :id => row[:rawid])
        
        last_modified row[:updated]
        data = Zlib::Inflate.inflate(row[:zdata])
        
        content_type :text

        begin
            JSON.parse(data)
            content_type :json
        rescue JSON::ParserError
        end

        data
    end

    
    head '/recipe/:source/:x/:id.:fmt' do |source,x,id,fmt|
        src = Kuiristo.source(source)
        row = DB[:recipes].select(:updated)
                          .first(:source => src::ID, :id => id.to_i)
        not_found if row.nil?  # Ensure data is found!
        not_found if ![ 'xml', 'html' ].include?(fmt)

        last_modified row[:updated]
    end

    get '/recipe/:source/:x/:id.:fmt' do |source,x,id,fmt|
        src = Kuiristo.source(source)
        row = DB[:recipes].select(:zxml, :updated)
                          .first(:source => src::ID, :id => id.to_i)
        not_found if row.nil?  # Ensure data is found!
        xmltxt = Zlib::Inflate.inflate(row[:zxml])
        
        last_modified row[:updated]

        case fmt
        when 'xml'
            content_type :xml
            xmltxt
            
        when 'html'
            # Recipe
            recipe = Kuiristo::Recipe.from_xml(xmltxt)

            # Recipe profil
            iprofils = DB[:recipe_ingredient]
                           .left_join(:ingredients, :id => :ingredient)
                           .where(:source => src::ID, :recipe => id.to_i)
                           .select(:profil)
                           .map(:profil)

            profil = VEGPROFILS.find(proc {[ 'unknown', 'Inconnu' ]}) {|k, v|
                iprofils.include?(k) }
            profil = [ 'ambiguous', 'Ambigu' ] if profil[0].nil?
                
            # Recipe categories
            tags     = DB[:recipe_category]
                           .left_join(:categories_for_recipes,
                                      :id => :category)
                           .where(:source => src::ID, :recipe => id)
                           .select(:id, :group, :name)
                           .all

            # Render
            title recipe.name
            erb :recipe, :locals => {
                :recipe    => recipe,
                :profil    => profil,
                :tags      => tags,
            }

        else
            not_found
        end
    end

end
