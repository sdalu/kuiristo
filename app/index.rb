class KuiristoWeb

    #
    # Index
    #
    
    get '/' do        
        # Request last updated recipies?
        last = if params[:last]
                   Integer(params[:last]) rescue 100
               end

        # Build user list with favorite recipes
        favorites  =
            DB[:user_favorite_recipe]
                .inner_join(:users, :id => :user)
                .all.group_by {|r| r[:user] }
                .each {|k,v| v.map!{|h| "#{h[:source]}/#{h[:recipe]}" } }
        users      =
            Hash[DB[:users].select(:id, :nickname).all
                     .select {|r| favorites.include?(r[:id]) }
                     .map!   {|r| [ r.delete(:id), r ] }]
        
        # Build category list
        categories_by_id =
            Hash[DB[:categories_for_recipes].order(:group, :id).all
                     .map! {|c| [c.delete(:id), c] }]
        
        # Build recipe list (joined with categories)
        lst  = DB[:recipes]
                   .left_join(:recipe_category,
                              Sequel[:recipes][:source] =>
                                Sequel[:recipe_category][:source],
                              Sequel[:recipes][:id    ] =>
                                Sequel[:recipe_category][:recipe])
                   .group(Sequel[:recipes][:source], Sequel[:recipes][:id])
                   .select(Sequel[:recipes][:id         ],
                           Sequel[:recipes][:source     ],
                           Sequel[:recipes][:name       ],
                           Sequel[:recipes][:robot      ],
                           Sequel[:recipes][:time_active],
                           Sequel[:recipes][:time_total ],
                           Sequel[:recipes][:portion    ],
                           Sequel[:recipes][:price      ],
                           Sequel[:recipes][:difficulty ],
                           Sequel[:recipes][:meta       ],
                           Sequel.function(:group_concat, :category)
                               .as(:categories))
                   .order(:name)

        #  ... limit to `last` elements if requested
        #        (ordered by time of rawdata, and added updated time)
        lst = lst.left_join(:rawdata, Sequel[:recipes][:source] =>
                                        Sequel[:rawdata][:source],
                                      Sequel[:recipes][:rawid] =>
                                        Sequel[:rawdata][:id])
                  .select_more(Sequel[:rawdata][:updated])
                  .order(Sequel.desc(Sequel[:rawdata][:updated]))
                  .limit(last) 					if last

        # ... cleanup recipe list
        lst = lst.all
        lst = lst.each {|r|
            meta  = MessagePack.unpack(r[:meta])
            imgs  = (meta&.dig('images') || []).map {|i,w,h|
                        { sha256: i, width: w, height: h } }

            # Image src
            src          = if !imgs.empty? &&
                              (score = $config.dig(:web, :index, :img_score))
                               Kuiristo::Recipe.best_image(imgs, score: score)
                           end

            # Image srcset / sizes
            sizes, limit = if !imgs.empty?
                               $config.dig(:web, :index, :srcset)
                           end
            srcset       = []
            if sizes
                srcset += imgs
                if limit
                    srcset.select! {|h| limit.include?(h[:width]) }
                    if srcset.empty?
                        lst = imgs.sort {|a,b| a[:width] <=> b[:width] }
                        i    = lst.find {|h| h[:width] > limit.end } ||
                               lst.last
                        srcset << i  # !imgs.empty? ensured to have an image
                    end
                end
            end

            # Update recipe info
            r.merge!(
                :image         => [ src, sizes, srcset ],
                :meta          => meta,
                :robot         => r[:robot].split(','),
                :categories    => (r[:categories]||'').split(',').map(&:to_i),
                :ladder_total  => time_ladder(r[:time_total ], :total),
                :ladder_active => time_ladder(r[:time_active], :active) )
        }
        
        # Min/Max for time (total, active)
        time_total  = lst.map{|r| r[:time_total ] }.minmax
        time_active = lst.map{|r| r[:time_active] }.minmax

        # Stats
        stats = {
            :recipe => {
                :count => lst.size
            },
            :user => {
                :count => users.size
            }
        }

        # Render view
        if last
            title "#{NAME} (last: #{last})"

            erb :index, :locals => {
                    recipes: lst,  
                 categories: categories_by_id,
                 kategories: CATEGORIES,
                      stats: stats,
                   grouping: false,
                       lazy: $config.dig(:web, :index, :lazy    ) &&
                             !is_crawler?                        ,
            key_accelerator: { visible: false },
                       view: $config.dig(:web, :index, :view    ),
                     search: { visible: false },
                     header: $config.dig(:web, :index, :header  ),
                     footer: $config.dig(:web, :index, :footer  ),
            }
        else
            title NAME

            # Recipe grouping
            if $config.dig(:web, :index, :grouping) != false
                lst = lst.group_by {|recipe|
                    # Use our own normalization method to deal with oe
                    #  We need to take again the first char as the
                    #  normalization can expend to more than one char
                    recipe[:name] =~ /\p{L}/u  # Find first letter
                    Kuiristo.normalize_string_ci($&)[0].upcase
                }

                # Ensure sorting is correct due to group by
                # not necessary using the first char (but the first letter)
                lst = Hash[lst.sort {|(a,_),(b,_)| a<=>b}]
            end
            
            ingredients = self.call(
                'REQUEST_METHOD' => 'GET',
                'PATH_INFO'      => '/search-ingredients.json',
                'rack.input'     => StringIO.new
            )[2].join('')
            
            erb :index, :locals => {
                    recipes: lst,  
                 categories: categories_by_id,
                 kategories: CATEGORIES,
                      users: users,
                  favorites: favorites,
                ingredients: ingredients,
                      stats: stats,
                       lazy: $config.dig(:web, :index, :lazy    ) &&
                             !is_crawler?                        ,
            key_accelerator: $config.dig(:web, :index, :index   ),
                       view: $config.dig(:web, :index, :view    ),
                     search: $config.dig(:web, :index, :search  ),
                     header: $config.dig(:web, :index, :header  ),
                     footer: $config.dig(:web, :index, :footer  ),
                }
        end
    end

end
