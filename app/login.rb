class KuiristoWeb

    #
    # Login
    #

    get '/login/' do
        erb :login, :locals => {
                :users   => DB[:users].all,
                :current => ruser
            }
    end

    post '/login/' do
        case params[:action]
        when 'login'       then session['user'] = params[:user]
        when 'logout'      then session.delete('user')
        when 'editing-on'  then session['editing'] = 'true'
        when 'editing-off' then session['editing'] = 'false'
        end
        "done"
    end
    
    get '/login/:user' do |user|
        session['user'] = user
    end

end
