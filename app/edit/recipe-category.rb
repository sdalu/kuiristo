class KuiristoWeb

    #
    # Category (aka Tag) editing
    #

    get '/recipe/:source/:x/:id/tag' do |source,x,id|
        content_type :json

        src = Kuiristo.source(source)
        DB[:recipe_category]
            .left_join(:categories_for_recipes, :id => :category)
            .where(:source => src::ID, :recipe => id)
            .select(:id, :group, :key, :name)
            .all.to_json
    end

    post '/recipe/:source/:x/:id/tag' do |source,x,id|
        src = Kuiristo.source(source)
        tag = params[:tag]
        tbl = DB[:recipe_category]
        key = { :source => src::ID, :recipe => id, :category => tag }
        case params[:op]
        when '+' then tbl.insert_ignore.insert(key)
        when '-' then tbl.where(key).delete
        else raise "unsupported operation"
        end
        true
    end

end
