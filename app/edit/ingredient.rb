class KuiristoWeb

    #
    # Ingredients
    #
    get '/ingredient/' do
        cat = Hash[DB[:categories_for_ingredients]
                       .order(:group, :id)
                       .map {|id:, **o| [ id, o ] } ]

        shp = Hash[DB[:categories_for_shopping]
                       .order(:id)
                       .map{ |id:, **o| [ id, o ] } ]
        
        lst = DB[:ingredients]
                  .left_join(:ingredient_category, :ingredient => :id)
                  .select(:id, :profil, :shopping,
                          Sequel.function(:coalesce, :singular, :plural)
                              .as(:name),
                          Sequel.function(:group_concat, :category)
                              .as(:categories))
                  .group(:id)

        lst = case params[:sort]
              when 'id'   then lst.order(:id)
              when 'name' then lst.order(:name)
              else             lst.order(:name)
              end

        lst = lst.all
                  
        title "Ingredients (edition) | #{NAME}"
        erb :'ingredients-editing', :locals => {
            :ingredients    => lst,
            :cat_shopping   => shp,
            :cat_ingredient => cat,
            :profils        => VEGPROFILS,
        }
    end

    post '/ingredient/:id' do |id|
        if profil   = params[:profil]
            profil = nil if profil == '-'
            DB[:ingredients].where(:id => id).update(:profil => profil) 
        end
        if shopping = params[:shopping]
            shopping = nil if shopping == '-'
            DB[:ingredients].where(:id => id).update(:shopping => shopping) 
        end
        if cat      = params[:category]
            tbl = DB[:ingredient_category]
            key = {:ingredient => id, :category => cat }
            case params[:op]
            when '+' then tbl.insert_ignore.insert(key)
            when '-' then tbl.where(key).delete
            else raise "unsupported operation"
            end
        end
        true
    end

end
