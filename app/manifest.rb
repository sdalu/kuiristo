class KuiristoWeb

    get '/manifest.json' do
        content_type 'application/manifest+json'
        { name: NAME,
          short_name: NAME,
          vesrsion: '0.1',
          description: 'Livre de recettes',
          theme_color: '#F5F5FF',
          background_color: '#EEEEEE',
          icons: [ { src: 'recipe.svg',
                     sizes: "48x48 72x72 96x96 128x128 256x256 512x512" }
                 ],
	  developer: {
	      name: "Your Name",
	      url: "http://yourawesomeapp.com"
	  },
          installs_allowed_from: ["*"],
          launch_path: to('/index.html'),
          appcache_path: to("/offline.appcache"),
          start_url: to('/'),
          display: 'standalone',
          orientation: 'portrait',
          scope: to('/'),
	  permissions: {
		systemXHR: {}
	}
        }.to_json
    end

end
