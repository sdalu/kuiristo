class KuiristoWeb

    #
    # 
    #
    
    get '/categories.json' do
        content_type :json

        DB[:categories_for_recipes]
            .select(:id, :key, :name, :group)
            .order(:group, :id)
            .all.to_json
    end


    # Typeahead? Fuse.js?
    # https://en.wikipedia.org/wiki/Bitap_algorithm
    get '/search-ingredients.json' do
        content_type :json
        
        # List of categories with break down names
        c = DB[:categories_for_ingredients]
                .select(:id, :name).all
                .map {|h| h => { id:, name: }
                       { :id     => "c/#{id}",
                         :name   => name,
                         :group  => 'category',
                         :search => Kuiristo.search_breaker(name) } }
        
        # List of ingredients with break down names
        i = DB[:ingredients]
                .select(:id, :name, :variant, :part,
                        Sequel.function(:coalesce, :singular, :plural)
                            .as(:fullname)).all
                .map {|h| h => { id:, fullname:, name:, variant:, part: }
                       { :id     => "i/#{id}",
                         :name   => fullname.capitalize,
                         :group  => 'ingredient',
                         :search => Kuiristo.search_breaker(
                             *[ name, variant&.split(/\s*,\s*/), part ]
                                  .flatten.compact) } }
        
        # List of ingredients groups with break down names
        ig = DB[:ingredients]
                .select(:name).group(:name).all
                .map {|h| h => { name: }
                       { :id     => "ig/#{name}",
                         :name   => name.capitalize,
                         :group  => 'ingredient-group',
                         :search => Kuiristo.search_breaker(name) } }
        
        # Recipies mapping with categories
        rc = DB[:categories_for_ingredients]
                 .inner_join(:ingredient_category,
                             Sequel[:ingredient_category][:category] =>
                             Sequel[:categories_for_ingredients][:id])
                 .inner_join(:recipe_ingredient,
                             Sequel[:recipe_ingredient][:ingredient] =>
                             Sequel[:ingredient_category][:ingredient])
                 .select(:id,
                         Sequel.function(:group_concat,
                                     Sequel.function(:concat,
                                                     :source, '/', :recipe))
                         .as(:recipes))
                 .group(:id)
                 .all
                 .map {|h| h => { id:, recipes: }
                        [ "c/#{id}", recipes.split(',') ] }

        # Recipies mapping with ingredients
        ri = DB[:ingredients]
                 .inner_join(:recipe_ingredient,
                             Sequel[:recipe_ingredient][:ingredient] =>
                             Sequel[:ingredients][:id])
                 .select(:id,
                         Sequel.function(:group_concat,
                                     Sequel.function(:concat,
                                                     :source, '/', :recipe))
                             .as(:recipes))
                 .group(Sequel[:ingredients][:id])
                 .all
                 .map {|h| h => { id:, recipes: }
                        [ "i/#{id}", recipes.split(',') ] }
        
        # Recipies mapping with ingredients group
        rig = DB[:ingredients]
                 .inner_join(:recipe_ingredient,
                             Sequel[:recipe_ingredient][:ingredient] =>
                             Sequel[:ingredients][:id])
                 .select(:name,
                         Sequel.function(:group_concat,
                                     Sequel.function(:concat,
                                                     :source, '/', :recipe))
                             .as(:recipes))
                 .group(Sequel[:ingredients][:name])
                 .all
                 .map {|h| h => { name:, recipes: }
                        [ "ig/#{name}", recipes.split(',') ] }
        
        # Matching / Recipes
        matching = c + ig + i
        recipes  = Hash[rc + ri + rig]

        # Remove ingredient for which we dont have recipes
        matching.reject! {|h| !recipes.include?(h[:id]) }
        
        # JSON
        { :matching => matching,
          :recipes  => recipes,
        }.to_json
    end

end
