
Demonstration
---------------
[![](readme/t/index-view.png)](readme/index-view.png)
[![](readme/t/ingredient-selection-and-basket.png)](readme/ingredient-selection-and-basket.png)
[![](readme/t/time-limit-and-thumbnails-view.png)](readme/time-limit-and-thumbnails-view.png)
[![](readme/t/shopping-list.png)](readme/shopping-list.png)

http://kuiristo.eu/

Il n'a que 2 recettes. Contribuez en envoyant vos recettes ou
en deposant des pull-request sur [kuiristo-yumi](https://gitlab.com/sdalu/kuiristo-yumi)


Fonctionnalites
---------------
* page d'index de l'ensemble des recettes
* marque page pour les favoris
* moteur de recherche par
  * temps (de preparation et total)
  * ingredients (avec et sans)
  * type de plat
  * type de preparation
  * profil (vegetarien, vegan, ...)
  * saison
  * origine (asie, inde, ...)
  * favoris
* creation d'une liste de course
* visualisation de la recette
* impression-friendly
* facilement exportable sous forme hors-ligne

Prerequis
---------
* ruby >= 2.4 (not tested on 2.3)
* libxml2
* ImageMagick
* mysql/mariadb
* pHash (optionel)


TODO
----
La fonte n'est pas utilisable dans un context commerciale, car
la majorite des icones provienent de:
* https://www.svgrepo.com/ (NC)
* https://pixabay.com/     (CC0)

=> remplacer la fonte avec une fonte dont les caracteres sont sous license SIL

Installation
------------

```sh
# Fetching sources
git clone --recursive http://gitlab.com/sdalu/kuiristo.git
git clone http://gitlab.com/sdalu/kuiristo-yumi.git

# Configuring
cd kuiristo                            # Moving to main directory
cp config.yaml.sample config.yaml      # Using default config
vi config.yaml                         # Editing configuration
bundle install --path vendor           # Installing dependencies
rake db:init                           # Initializing database
```

```sh
# Import des recettes Yumi
rake import[yumi]                      # Import (fetch, build, tag)

# ou ...
rake fetch:index[yumi]                 # Fetch index
rake fetch:recipe[yumi]                # Fetch recipes
rake fetch:dependencies[yumi]          # Fetch dependencies (images)
rake db:build:recipe[yumi]             # Parse and build recipes
rake db:build:index:ingredients[yumi]  # Create ingredient index
rake db:build:image:phash              # Compute images phash
rake db:autotag[yumi]                  # Perform auto-tagging
```


```sh
# Start web service
rake web:start
```

Création d'une version hors-ligne
```sh
wget --restrict-file-names=windows --no-check-certificate --mirror --convert-links --adjust-extension --page-requisites --no-parent https://website/kuiristo/

sed -i '' -e 's/index.html\#/\#/g' index.html
```

Exemples de recettes
--------------------
Le fichier de recette est décrit par le schema kuiristo.xsd dont l'explication est fournit sur la page kuiristo.html

```xml
<?xml version="1.0" encoding="UTF-8"?>
<k:recipe xmlns:k="urn:kuiristo" xmlns="http://www.w3.org/1999/xhtml"
    id        = "201709190001"
    name      = "Jus d’orange"
    robot     = "TM31,TM5"
    price     = "low"
    difficulty= "easy"
    portion   = "4">

  <k:time active="600" total="600"/>

  <k:image src="jus-d'orange.jpg"/>

  <k:group>
    <k:ingredient id="227" quantity="10" unit="unit" notation="oranges"
		   preparation="épluchées et coupées en morceaux"/>
    <k:step>Mettre les oranges dans le bol et pulvériser
      <k:tts time="40" speed="10"/>.</k:step>
  </k:group>

  <k:info type="tip">
    Pour enlever la mousse vous pouvez passer le jus
    au chinois ou à la passoire.
  </k:info>

  <k:nutrition quantity="1"   unit="portion" energy="980"/>
  <k:nutrition quantity="100" unit="gramme"  energy="200"/>
</k:recipe>
```
