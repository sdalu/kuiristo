
/*== Starred recipes ===================================================*/

    
$(function() {
if ($('.recipe[data-source][data-recipe]').length > 0) {
    var orig   = $('.recipe[data-source][data-recipe]');
    var source = orig.data('source');
    var recipe = orig.data('recipe');
    var url    = recipe + '/starred';
    
    $.getJSON(url).done(function(data) {
	$('<div class="starred web-only"/>')
	    .append($('<i class="fas fa-fw fa-star"/>'))
	    .append($('<i class="far fa-fw fa-star"/>'))
	    .toggleClass('selected', data)
	    .appendTo($('.recipe-preview .holder .recipe-annotation'))
	    .css('cursor', 'pointer')
	    .on('click', function(evt) {
		var me  = $(this).addClass('updating');
		var sel = $(this).hasClass('selected');
		var op  = sel ? '-' : '+';
		$.post(url, { op: op }).done(function(data) {
		    me.toggleClass('selected', !sel).removeClass('updating');
		});
	    });
    });
}
});




/*== Starred index =====================================================*/

    
$(function() { // On page loaded
if ($('.recipe-list').length > 0) {
    var list     = $('.recipe-list');
    var item_sel = '.recipe-item[data-source][data-recipe]';

    list.on('click', item_sel + ' .starred', function(evt) {
	var entry  = $(this).closest('.recipe-item');
	var idhash = entry.data('idhash');
	var me     = $(this).addClass('updating');
	var sel    = $(this).hasClass('selected');
	var op     = sel ? '-' : '+';
	var url    = 'recipe/' + idhash + '/starred';
	$.post(url, { op: op }).done(function(data) {
	    me.toggleClass('selected', !sel).removeClass('updating');
	});
    });
    
    $.getJSON('recipe/starred').done(function(data) {
	var starred  = new Set();
	data.forEach(function(elt) {
	    starred.add(elt.source + '/' + elt.recipe); });
	
	$(item_sel, list).each(function() {
	    var id         = $(this).data('id');
	    var is_starred = starred.has(id);
	    
	    $('<div class="starred web-only"/>')
		.append($('<i class="fas fa-fw fa-star"/>'))
		.append($('<i class="far fa-fw fa-star"/>'))
		.toggleClass('selected', is_starred)
		.css('cursor', 'pointer')
		.appendTo(this);
	});
    });    
}
});
