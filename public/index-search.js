/*== Polyfill ==========================================================*/

if (Set.prototype.intersection == null) {
    Set.prototype.intersection = function(setB) {
	var intersection = new Set();
	for (var elem of setB)
            if (this.has(elem)) intersection.add(elem);
	return intersection;
    }
}

if (Set.prototype.intersect == null) {
    Set.prototype.intersect = function(setB) {
	for (var elem of setB)
            if (this.has(elem)) return true;
	return false;
    }
}

if (Set.prototype.union == null) {
    Set.prototype.union = function(setB) {
	var union = new Set(this);
	for (var elem of setB) {
            union.add(elem);
	}
	return union;
    }
}




/*== Search engine =====================================================*/

(function() { // Scoping

//
// Update text for #group-time input element
//
function time_range_update_text(elt) {
    var val    = $(elt).val();
    var ladder = $(elt).data('ladder').split(',');
    var value  = $(elt).closest('label').find('.value');
    var txt    = (val >= ladder.length)
	       ? 'Pas de limites'
	       : '< ' + formatHHMM(parseInt(ladder[val],10));
    value.text(txt);
}

    
//
// Convert minutes to hh:mm format
//
function formatHHMM(mins) {
    var hours = Math.floor(mins / 60);
    var mins  = mins % 60;
    var data  = [ hours, mins ].map(function(d){ return d>9 ? d : ('0'+d); });
    return data.join(':');
}

    
//
//  Full search root element
//
var fullsearch = $('#full-search');

    
//
// Peform a search and update the result
//
var action = 0;

function search_update() {
    clearTimeout(action);

    var times = {};
    if ($('.search-tab [name=group-selector][value=time]',
	  fullsearch).is(':checked')) {
	$('#group-time input[type=range]').each(function() {
	    var val    = $(this).val();
	    var name   = this.name;
	    var ladder = $(this).data('ladder').split(',');
	    var value  = $(this).closest('label').find('.value');
	    if (val >= ladder.length) return;
	    var time   = parseInt(ladder[val], 10) * 60;
	    times[name] = time;
	});
    }

    var cating_with;
    $('#group-ingredients input[name=ingredient-with]')
	.val().split(',').forEach(function(key) {
	    if (key.length > 0) {
		var set    = $INGREDIENTS.recipes[key] || new Set();
		var cating = cating_with;
		cating_with   = cating ? cating.intersection(set) : set;
	    }
	});
	
    var cating_without;
    $('#group-ingredients input[name=ingredient-without]')
	.val().split(',').forEach(function(key) {
	    if (key.length > 0) {
		var set    = $INGREDIENTS.recipes[key] || new Set();
		var cating = cating_without;
		cating_without = cating ? cating.intersection(set) : set;
	    }
	});
	
    
    var favs;
    if ($('.search-tab [name=group-selector][value=favorite]',
	  fullsearch).is(':checked')) {
	$('#group-favorite input[name=favorite][type=checkbox]:checked').each(function() {
	    var val    = $(this).val();
	    if (favs) {
		//favs = favs.union($USERS[val].favorites);
		favs = favs.intersection($FAVORITES[val]);
	    } else {
		favs = $FAVORITES[val];
	    }
	});
    }
    
    var data = $('#full-search .search-category :input').serializeArray();
    var list = {};
  
    data.forEach(function(elt) {
      var k = elt.name;
      var i = parseInt(elt.value, 10);
      i = Number.isNaN(i) ? elt.value : i;
      if (list[k] == null) list[k] = new Set();
      list[k].add(i);
    });

    s = []; // show
    h = []; // hide
    
    $('.recipe-item').each(function() {
	var elt  = this;
	var id   = $(this).data('id');
	
	// Save recipe category as set
	// Try to convert number-string to number
	var cats = $(this).data('categories-set');
	if (cats == null) {
	    cats = new Set($(elt).attr('data-categories').split(' ')
                           .map(function(c) {
			       var k = parseInt(c, 10);
			       return Number.isNaN(k) ? c : k;
			   }));
	    $(this).data('categories-set', cats);
	}
	
	var ok = true;
	for (k in list )  { ok= ok && list[k].intersect(cats);                }
	for (k in times)  { ok= ok && parseInt($(elt).data(k),10) < times[k]; }
	if (favs)         { ok= ok && favs.has(id);                           }
	if (cating_with)  { ok= ok && cating_with.has(id);                    }
	if (cating_without){ok= ok && !cating_without.has(id);                }
	
	(ok ? s : h).push(elt);
    });

    h.forEach(function(elt) { elt.classList.add('hidden');    })
    s.forEach(function(elt) { elt.classList.remove('hidden'); })

    
    $('.recipe-list dt.accel-key').each(function() {
	var items    = $(this).nextUntil(':not(dd.recipe-item)');
	var id       = $(this).attr('id');
	var hidden   = items.filter(':visible').length == 0;
	var accelkey = $(this);
	var dummy    = items.last().nextUntil('dt.accel-key')
	                           .filter('dd.dummy-expander');
	accelkey.toggleClass('hidden', hidden);
	dummy   .toggleClass('hidden', hidden);
    });
}
    
function schedule_search_update() {
    clearTimeout(action);
    action = setTimeout(search_update, 300)
}


$(search_update);



    
//
// If a link is clicked show/hide the corresponding pane
// If nothing is to be shown also hide the tab-content    
//
$('.search-tab', fullsearch).on('click', '.nav-link > a', function(evt) {
    evt.preventDefault();
    var navtarget   = $($(this).attr('href'));
    var navlink     = $(this).closest('.nav-link');
    var tabcontent  = false;
    
    if (navlink.hasClass('selected')) {
	navtarget.addClass('hidden');
	navlink.removeClass('selected');
	tabcontent = true;
    } else {
	var curlink   = navlink.siblings('.selected')
	var curtarget = $($('> a', curlink).attr('href'));
	navtarget.removeClass('hidden');
	navlink.addClass('selected');
	curtarget.addClass('hidden');
	curlink.removeClass('selected');	   
    }      

    $('.search-tab-content', fullsearch).toggleClass('hidden', tabcontent);
});


//
// If the search tab is switched off,
// ensure that we disable all the associated searches, and update search
//
$('.search-tab', fullsearch)
  .on('change', '.nav-link :input[type=checkbox]', function(evt) {
    if (!$(this).prop('checked')) {
	var group = $('.search-tab-content [id=group-'+this.value+']',
		      fullsearch);
	if (group.hasClass('search-category')) {
	    $(':input', group).prop('checked', false);
	} else if (group.is('#group-time')) {
	    $(':input', group).each(function() {
		$(this).val(this.defaultValue);
		time_range_update_text(this);
	    });
	} else if (group.is('#group-ingredients')) {
	    $(':input[type=search]', group).each(function() {
		this.selectize.clear(true);
	    });
	} else {
	    var input = $(':input', group);
	    input.val(function() {
		switch (this.type){
		case 'checkbox':
		case 'radio':
                    this.checked = this.defaultChecked;
		    break;
		default:
		    return this.defaultValue;
		}
	    });
	}
	schedule_search_update();
    }
});

    
//
// If an item of the search tab pane is changed,
// adjust (on/off -if possible-) the corresponding tab and update search
//
$('.search-tab-content', fullsearch)
  .on('change', '.search-tab-pane :input', function(evt) {
    var tabpane  = $(this).closest('.search-tab-pane');
    var link     = $('.search-tab [href=\'#' + tabpane.attr('id') + '\']',
		     fullsearch);
    var navlink  = $(link).closest('.nav-link');
    var selector = $('[name=group-selector]', navlink);
    var checked  = true;

    if (tabpane.data('managed') == 'no') {
	return;
    }
    if (tabpane.hasClass('search-category')) {
	checked  = tabpane.find(':checked').length > 0;
    } else if (tabpane.is('#group-time')) {
	var maxed = true;
	tabpane.find('input[type=range]').each(function() {
	    maxed = maxed && ($(this).val() == $(this).attr('max'));
	});
	checked = !maxed;
    } else if (tabpane.is('#group-favorite')) {
	checked  = tabpane.find(':checked').length > 0;	
    } else if (tabpane.is('#group-ingredients')) {
	var nosearch = true;
	tabpane.find(':input[type=search]').each(function() {
	    nosearch = nosearch && ($(this).val().length == 0);
	});
	checked = !nosearch;
    }
      
    selector.prop('checked', checked);
    schedule_search_update();
});


//
// Update textual representation of time range,
// and force immediate text update (not triggering change event
// as this would result in a search update)    
//
$('#group-time input[type=range]', fullsearch).change(function(evt) {
    time_range_update_text(this);
}).each(function() {
    time_range_update_text(this);
});


//
//


$('.search-tab').on('click', '.disabled', function(evt) {
    evt.preventDefault();
    evt.stopPropagation();
});



//
// Selectize
//
var options = {
    plugins          : [ 'remove_button' ],
    maxOptions       : null,
    maxItems         : null,
    highlight        : true,
    
    searchField      : [ 'name', 'search' ],
    sortField        : 'name',
    valueField       : 'id',
    labelField       : 'name',
    create           : false,
    hideSelected     : true,
    closeAfterSelect : true,
    openOnFocus      : false,
    
    lockOptgroupOrder: true,
    _optgroups       : [
	{ value: 'category',         label: 'Catégorie'                 },
	{ value: 'ingredient-group', label: 'Groupement d\'ingrédients' },
	{ value: 'ingredient',       label: 'Ingrédient précis'         }
    ],
    
    options          : $INGREDIENTS.matching,
    
    render           : {
        item: function(item, escape) {
	    return '<div class="group-' + escape(item.group) + '">' +
                     '<span>' + escape(item.name) + '</span>' +
		   '</div>';
        },
        option: function(item, escape) {
	    return '<div class="group-' + escape(item.group) + '">' +
                     '<span>' + escape(item.name) + '</span>' +
		   '</div>';
        }
    },
}
    
$('#group-ingredients input[type=search]', fullsearch)    
	.selectize(options);


})();
