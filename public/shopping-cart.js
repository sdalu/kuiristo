$(function() {
if (($('.recipe[data-id]').length > 0) || ($('.recipe-list').length > 0)) {

    var recp   = $('.recipe[data-id]');
    var id     = recp.data('id');
    var cart   = {};
    var user   = $('html').data('kuiristo-user');
    var root   = $('html').data('kuiristo-root');
    if (root.endsWith('/')) { // avoid building a protocol-less url
	root = root.slice(0, -1);
    }
    
    var summary       = $('<div class="cart-summary"/>');

    var checkout_mail = $('<div class="cart-checkout" data-format="mail"/>')
	.attr('title', 'Envoyer la liste des courses par e-mail')
	.append($('<i class="fas fa-paper-plane"/>'));
    
    var checkout_web  = $('<div class="cart-checkout" data-format="html"/>')
	.attr('title', 'Afficher la liste des courses')
	.append($('<i class="fas fa-shopping-cart"/>'));

    var banner = $('<summary class="cart-banner"/>').append(summary);
    if (user) { banner.append(checkout_mail); }
    banner.append(checkout_web);
    banner.on('click', '.cart-checkout', function(evt) {
	    evt.preventDefault();
	    evt.stopPropagation();

	    var format = $(this).data('format');
	    
	    var data   = [];
	    Object.keys(cart).forEach(function(key) {
		data.push(key.replace('/', '-') + '-' + cart[key].count);
	    });

	    window.location = root + '/shopping/list.' + format + '?for=' +
		encodeURIComponent(data.join('|'));
	    
	    /*
	    var data = [];
	    Object.keys(cart).forEach(function(key) {
		data.push([key, cart[key].count]);
	    });
	    f = $('<form/>')
		.attr('method',  'POST')
		.attr('enctype', 'application/json')
		.attr('action',  '/kuiristo/shopping/')
		.append($('<input/>')
			.attr('name',  'data')
			.attr('value', JSON.stringify(data)))
		.appendTo('body');
	    f.submit();
	    */
	    
	    /*
	    $.post({
		method: 'POST',
		url : '/kuiristo/shopping/',
		dataType: 'json',
		contentType: 'application/json',
		data: JSON.stringify(data),
		async: false,
	    });
	    */
	});





    
    var list  = $('<div class="cart-list"/>')
	.on('click', '.cart-item .fa-plus', function(evt) {
	    var id = $(this).closest('.cart-item').data('id');
	    var item = cart[id];
	    ++item.count;
	    update();
	})
	.on('click', '.cart-item .fa-minus', function(evt) {
	    var id = $(this).closest('.cart-item').data('id');
	    var item = cart[id];
	    if (--item.count <= 0)
		delete cart[id];
	    update();
	})
	.on('click', '.cart-empty', function(evt) {
	    cart = {};
	    update();
	});
    
    var holder = $('<details class="cart-holder sticky web-only"/>')
	.toggleClass('hidden', Object.keys(cart).length <= 0)
	.append(banner).append(list)
	.prependTo('body');



    if ($('.recipe[data-id]').length > 0) {
	var add = $('<div class="cart-item-add web-only"/>')
	    .attr('title', 'Ajouter au panier')
	    .append($('<i class="fas fa-cart-plus"/>'))
	    .click(function(evt) {
		var item = cart[id] || {
	  	    title: $('h1.title a', recp).text(),
 	             link: window.location.href,
  		       id: id,
	  	  picture: $('img.recipe-picture', recp).attr('src'),
		    count: 0
		};

		++item.count;
		cart[id] = item;
		update();
	    });
	
	$('h1.title').append(add);
    }


    function update(save) {
	if (save !== false) {
	    localStorage.setItem('kuiristo_cart', JSON.stringify(cart));
	}

	if (id) {
	    $('h1.title .cart-item-add')
		.attr('data-count', cart[id] ? cart[id].count : '');
	}
	
	holder.toggleClass('hidden', Object.keys(cart).length <= 0);
	summary.text(Object.keys(cart).length + " Recettes");
	list.empty();
	Object.keys(cart).forEach(function(key) {
	    var item = cart[key];
	    var obj  = $('<div/>').addClass('cart-item')
		.attr('data-id', key)
		.append($('<a class="item-entry"/>')
			.append($('<span class="item-picture"/>')
				.append($('<img/>').attr('src', item.picture)))
			.append($('<span class="item-title"/>')
				.text(item.title))
			.append($('<div class="item-spacer"/>'))
			.attr('href', item.link)
		       )
		.append($('<div class="item-count"/>').text(item.count))
		.append($('<i class="fas fa-minus"/>'))
		.append($('<i class="fas fa-plus"/>'))
	    ;
	    list.append(obj);
	});
	var empty = $('<div class="cart-empty"/>')
	    .append($('<span/>').text('Vider le panier'))
	    .append($('<i class="fas fa-fw fa-trash"/>'));
	list.append(empty);
    }

    $(window).bind('storage', function (e) {
	cart = JSON.parse(localStorage.getItem('kuiristo_cart') || '{}');
	
	// Storage only fire
	//   - on open-tabs except the current one
	//   - if the value is not the same    
	update(false);
    }).trigger('storage');


}
});
