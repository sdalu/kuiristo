/* Permanently ignore checked items (removing them)
 */
$(function() {
    
var eraser = $('<div class="hover-scale"/>')
    .css('cursor', 'pointer')
    .css('margin-left', '1ex')
    .attr('title', 'Supprime les éléments cochés')
    .append($('<i class="fas fa-cut"/>'))
    .appendTo('.shopping-list h1.title')
    .click(function() {
	eraser.remove_items();
	eraser.check_visibility();
	    
	history.replaceState(null, document.title,
			     eraser.get_updated_location());
    });

eraser.check_visibility = function() {
    var state = $('.ingredients-list input:checked').length <= 0;
    eraser.toggleClass('hidden', state);
};

eraser.remove_items = function() {
    var ilist = $('.ingredients-list');
    $('input:checked', ilist).closest('li').remove();
    $('dt', ilist).each(function() {
	var dt = $(this);
	dt.nextUntil(':not(dd)').each(function() {
	    if ($('li', this).length == 0)
		    $(this).remove();
	})
	if (dt.nextUntil(':not(dd)').length == 0) {
	    dt.remove();
	}
    });
};

eraser.get_updated_location = function() {
    var url     = new URL(window.location);
    var ignored = new Set();
	
    for (const i of [ 'ignored', 'checked' ]) {
	for (const p of url.searchParams.getAll(i))
	    for (const e of p.split('|')) 
		ignored.add(e);
	url.searchParams.delete(i);
    }
    
    url.searchParams.append('ignored', [...ignored].join('|'));
    
    return url.toString();
}
    

    
$('.ingredients-list input:checkbox').change(eraser.check_visibility);
eraser.check_visibility();

});
    


/* Allow sending list by email
 */
$(function() {

var user   = $('html').data('kuiristo-user');

if (! user) { return; }
    
var mailer = $('<div class="hover-scale"/>')
    .css('cursor', 'pointer')
    .css('margin-left', '1ex')
    .attr('title', 'Envoyer la liste par e-mail')
    .append($('<i class="fas fa-paper-plane"/>'))
    .appendTo('.shopping-list h1.title')
    .click(function() {
	var url = new URL(window.location);
	url.pathname = url.pathname.replace(/\.html$/, '.mail');

	window.location = url;
    });
});




/* Alter URL to reflect shopping list status (checked items)
 */
$(function() {

$('.ingredients-list input:checkbox').change(function() {
    var iid     = $(this).closest('[data-ingredient-id]')
                         .data('ingredient-id');
    var url     = new URL(window.location);
    var checked = new Set();

    for (var p of url.searchParams.getAll('checked')) {
        for (var e of p.split('|')) {
	    var val = parseInt(e, 10);
	    if (! Number.isNaN(val)) 
		checked.add(val);
        }
    }
    
    if (this.checked) { checked.add(iid);    }
    else              { checked.delete(iid); }
    
    url.searchParams.delete('checked');
    url.searchParams.append('checked', [...checked].join('|'));
    
    history.replaceState(null, document.title, url.toString());
});

});
