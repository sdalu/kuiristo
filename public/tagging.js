

/*== Tagging recipes ===================================================*/

$(function() {
if ($('article.recipe[data-id]').length > 0) {

    var root      = $('html').data('kuiristo-root');

    var recipe_id = $('article.recipe[data-recipe]').data('recipe');
    var tag_url   = recipe_id + '/tag';

    var loading   = $('<div class="loading-message"/>')
	               .text('Chargement des tags')
	               .prepend($('<div class="loading-spinner-bar"/>'));
    var tagging   = $('<div class="tagging sticky web-only"/>')
	               .on('click', '.tag', function(evt) {
			   var me     = $(this);
			   var tag_id = $(this).data('id');
			   var on     = $(this).hasClass('on');
			   var op     = on ? '-' : '+';
			   me.addClass('updating');
			   $.post(tag_url, { op: op, tag: tag_id })
			       .done(function() {
				   me.removeClass('updating');
				   me.toggleClass('on', !on);
			       });
		       })
    	               .insertBefore($('article.recipe .recipe-preview'))
    	               .addClass('loading').append(loading);

    var tags      = new Set();
    $('article.recipe footer .recipe-tags .tag[data-id]')
	.each(function() { tags.add($(this).data('id')); });

    
    $.getJSON(root + '/categories.json').done(function(cats) {
	tagging.empty().removeClass('loading');
	    
	cats.forEach(function(e) {
	    $('<span class="tag"/>')
		.toggleClass('on', tags.has(e.id))
		.attr('data-id', e.id)
		.addClass('recipe-' + (e.group || 'unknown'))
		.text(e.name)
		.appendTo(tagging);
	})

    });

}
});
