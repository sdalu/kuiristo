Sequel.migration do
  change do
    create_table(:categories_for_ingredients) do
      primary_key :id, :type=>"smallint(5) unsigned"
      column :key, "varchar(32)", :null=>false, :collate=>"ascii_general_ci"
      column :name, "varchar(24)", :collate=>"utf8mb4_unicode_520_ci"
      column :group, "varchar(32)", :collate=>"ascii_general_ci"
      
      index [:key], :unique=>true
    end
    
    create_table(:categories_for_shopping) do
      primary_key :id, :type=>"tinyint(3) unsigned"
      column :key, "varchar(32)", :null=>false, :collate=>"ascii_general_ci"
      column :name, "varchar(48)", :collate=>"utf8mb4_unicode_520_ci"
      
      index [:key], :unique=>true
    end
    
    create_table(:images) do
      column :data, "mediumblob", :null=>false
      column :weight, "int(10) unsigned", :null=>false
      column :format, "enum('jpeg','png')", :null=>false, :collate=>"ascii_general_ci"
      column :width, "smallint(5) unsigned", :null=>false
      column :height, "smallint(5) unsigned", :null=>false
      column :sha256, "binary(32)", :null=>false
      column :phash, "bigint(20) unsigned"
      foreign_key :derivative, :images, :type=>"binary(32)", :key=>[:sha256]
      column :inserted, "timestamp", :default=>Sequel::CURRENT_TIMESTAMP, :null=>false
      column :source, "tinyint(3) unsigned", :null=>true
      
      primary_key [:sha256]
      
      index [:derivative], :name=>:derivative
    end
    
    create_table(:rawdata) do
      column :source, "tinyint(3) unsigned", :null=>false
      column :id, "bigint(20) unsigned", :null=>false
      column :key, "varchar(128)", :collate=>"utf8_bin"
      column :zdata, "mediumblob"
      column :rejected, "varchar(16)", :collate=>"ascii_general_ci"
      column :updated, "timestamp", :default=>Sequel::CURRENT_TIMESTAMP, :null=>false
      column :dependencies, "timestamp", :null=>true
      column :processed, "timestamp", :null=>true
      
      primary_key [:source, :id]
      
      index [:dependencies], :name=>:dependencies
      index [:processed], :name=>:processed
      index [:rejected], :name=>:rejected
      index [:updated], :name=>:updated
    end
    
    create_table(:users) do
      column :id, "varchar(16)", :null=>false, :collate=>"ascii_general_ci"
      column :nickname, "varchar(16)", :null=>false, :collate=>"utf8mb4_general_ci"
      column :email, "varchar(64)", :collate=>"ascii_general_ci"
      
      primary_key [:id]
    end
    
    create_table(:ingredients) do
      column :id, "int(10) unsigned", :null=>false
      column :singular, "varchar(64)", :collate=>"utf8mb4_unicode_520_ci"
      column :plural, "varchar(64)", :collate=>"utf8mb4_unicode_520_ci"
      column :name, "varchar(48)", :collate=>"utf8mb4_unicode_520_ci"
      column :variant, "varchar(48)", :collate=>"utf8mb4_unicode_520_ci"
      column :part, "varchar(32)", :collate=>"utf8mb4_unicode_520_ci"
      column :size, "enum('small','medium','big')", :collate=>"ascii_general_ci"
      column :mix, "tinyint(1)"
      column :maturity, "enum('half-ripe','ripe','well-ripe')", :collate=>"ascii_general_ci"
      column :skin, "tinyint(1)"
      column :salted, "tinyint(1)"
      column :sugared, "tinyint(1)"
      column :fresh, "tinyint(1)"
      column :treatment, "enum('untreated','organic')", :collate=>"ascii_general_ci"
      column :preserving, "set('brine','salt','confit','oil','syrup','smoked','dried','vinegar')", :collate=>"ascii_general_ci"
      column :processing, "set('trimmed','shelled','boned','pitted','cleaned','drawn','dressed','denervated','disgorged')", :collate=>"ascii_general_ci"
      column :shape, "enum('whole','powder','flakes','grated','shredded','slices','pieces','chopped','ground','minced','puree','diced','chips','tournedos','cut','ball', 'puffed')", :collate=>"ascii_general_ci"
      column :cooking, "enum('raw','pre-cooked','half-cooked','cooked','grilled')", :collate=>"ascii_general_ci"
      column :packaging, "enum('canned','frozen','jar','bag')", :collate=>"ascii_general_ci"
      column :profil, "enum('omnivore','semi-vegetarian','vegetarian','vegan')", :collate=>"ascii_general_ci"
      foreign_key :shopping, :categories_for_shopping, :type=>"tinyint(4) unsigned", :key=>[:id]
      
      primary_key [:id]
      
      index [:shopping], :name=>:shopping
    end
    
    create_table(:key_image) do
      column :source, "tinyint(3) unsigned", :null=>false
      column :key, "varbinary(256)", :null=>false
      foreign_key :sha256, :images, :type=>"binary(32)", :null=>false, :key=>[:sha256]
      
      primary_key [:source, :key]
      
      index [:sha256], :name=>:sha256
    end
    
    create_table(:recipes) do
      column :source, "tinyint(3) unsigned", :null=>false
      column :id, "bigint(20) unsigned", :null=>false
      column :rawid, "bigint(20) unsigned", :null=>false
      column :zxml, "blob", :null=>false
      column :name, "varchar(128)", :null=>false, :collate=>"utf8mb4_unicode_520_ci"
      column :robot, "set('TM5','TM31','TM21')", :null=>false, :collate=>"ascii_general_ci"
      column :time_robot, "mediumint(8) unsigned", :default=>0, :null=>false
      column :time_active, "mediumint(8) unsigned", :default=>0, :null=>false
      column :time_waiting, "mediumint(8) unsigned", :default=>0, :null=>false
      column :time_baking, "mediumint(8) unsigned", :default=>0, :null=>false
      column :time_total, "mediumint(8) unsigned", :default=>0, :null=>false
      column :portion, "smallint(5) unsigned", :null=>false
      column :price, "set('high','medium','low')", :collate=>"ascii_general_ci"
      column :difficulty, "set('advanced','medium','easy')", :collate=>"ascii_general_ci"
      column :meta, "blob"
      column :updated, "timestamp", :default=>Sequel::CURRENT_TIMESTAMP, :null=>false
      
      primary_key [:source, :id]
      foreign_key [:source, :rawid], :rawdata, :name=>:recipes_ibfk_1, :key=>[:source, :id]
      
      index [:difficulty], :name=>:difficulty
      index [:name], :name=>:name
      index [:portion], :name=>:portion
      index [:price], :name=>:price
      index [:robot], :name=>:robot
      index [:source, :rawid], :name=>:source_rawid, :unique=>true
      index [:time_active], :name=>:time_active
      index [:time_total], :name=>:time_total
    end
    
    create_table(:categories_for_recipes) do
      column :id, "smallint(5) unsigned", :null=>false
      column :group, "varchar(16)", :collate=>"ascii_general_ci"
      column :key, "varchar(32)", :null=>false, :collate=>"ascii_general_ci"
      column :name, "varchar(24)", :collate=>"utf8mb4_unicode_520_ci"
      foreign_key :image, :key_image, :type=>"binary(32)", :key=>[:sha256]
      column :icon, "varchar(32)", :collate=>"ascii_bin"
      
      primary_key [:id]
      
      index [:group, :key], :name=>:group_key, :unique=>true
      index [:image], :name=>:image
    end
    
    create_table(:ingredient_category) do
      foreign_key :ingredient, :ingredients, :type=>"int(10) unsigned", :null=>false, :key=>[:id]
      foreign_key :category, :categories_for_ingredients, :type=>"smallint(10) unsigned", :null=>false, :key=>[:id]
      
      primary_key [:ingredient, :category]
      
      index [:category], :name=>:ingredient_category_ibfk_1
    end
    
    create_table(:ingredient_mapping) do
      column :source, "tinyint(3) unsigned", :null=>false
      column :id, "bigint(20) unsigned", :null=>false
      foreign_key :ingredient, :ingredients, :type=>"int(10) unsigned", :null=>false, :key=>[:id]
      column :approximated, "tinyint(1)"
      
      primary_key [:source, :id]
      
      index [:ingredient], :name=>:ingredient
    end
    
    create_table(:recipe_ingredient) do
      column :source, "tinyint(3) unsigned", :null=>false
      column :recipe, "bigint(20) unsigned", :null=>false
      foreign_key :ingredient, :ingredients, :type=>"int(10) unsigned", :null=>false, :key=>[:id]
      column :quantities, "tinytext", :collate=>"utf8mb4_bin"
      
      primary_key [:source, :recipe, :ingredient]
      foreign_key [:source, :recipe], :recipes, :key=>[:source, :id]
      
      index [:ingredient], :name=>:recipe_ingredient_ibfk_2
    end
    
    create_table(:user_favorite_recipe) do
      foreign_key :user, :users, :type=>"varchar(16)", :null=>false, :key=>[:id], :collate=>"ascii_general_ci"
      column :source, "tinyint(3) unsigned", :null=>false
      column :recipe, "bigint(20) unsigned", :null=>false
      
      primary_key [:user, :source, :recipe]
      foreign_key [:source, :recipe], :recipes, :key=>[:source, :id]
      
      index [:source, :recipe], :name=>:source
    end
    
    create_table(:recipe_category) do
      column :source, "tinyint(3) unsigned", :null=>false
      column :recipe, "bigint(20) unsigned", :null=>false
      foreign_key :category, :categories_for_recipes, :type=>"smallint(5) unsigned", :null=>false, :key=>[:id]
      
      primary_key [:source, :recipe, :category]
      foreign_key [:source, :recipe], :recipes, :key=>[:source, :id]
      
      index [:category], :name=>:category
    end
  end
end
