-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 04, 2017 at 03:01 PM
-- Server version: 10.2.9-MariaDB-log
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kuiristo`
--
CREATE DATABASE IF NOT EXISTS `kuiristo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `kuiristo`;

-- --------------------------------------------------------

--
-- Table structure for table `categories_for_ingredients`
--

CREATE TABLE `categories_for_ingredients` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `key` varchar(32) CHARACTER SET ascii NOT NULL,
  `name` varchar(24) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `group` varchar(32) CHARACTER SET ascii DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories_for_recipes`
--

CREATE TABLE `categories_for_recipes` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `group` varchar(16) CHARACTER SET ascii DEFAULT NULL,
  `key` varchar(32) CHARACTER SET ascii NOT NULL,
  `name` varchar(24) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `image` binary(32) DEFAULT NULL,
  `icon` varchar(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories_for_shopping`
--

CREATE TABLE `categories_for_shopping` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `key` varchar(32) CHARACTER SET ascii NOT NULL,
  `name` varchar(48) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `data` mediumblob NOT NULL,
  `weight` int(10) UNSIGNED NOT NULL,
  `format` enum('jpeg','png') CHARACTER SET ascii NOT NULL,
  `width` smallint(5) UNSIGNED NOT NULL,
  `height` smallint(5) UNSIGNED NOT NULL,
  `sha256` binary(32) NOT NULL,
  `phash` bigint(20) UNSIGNED DEFAULT NULL,
  `derivative` binary(32) DEFAULT NULL COMMENT 'Derivative work of',
  `inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `source` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Source of first insertion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Images';

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(10) UNSIGNED NOT NULL,
  `singular` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `plural` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `name` varchar(48) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `variant` varchar(48) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `part` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `size` enum('small','medium','big') CHARACTER SET ascii DEFAULT NULL,
  `mix` tinyint(1) DEFAULT NULL,
  `maturity` enum('half-ripe','ripe','well-ripe') CHARACTER SET ascii DEFAULT NULL,
  `skin` tinyint(1) DEFAULT NULL,
  `salted` tinyint(1) DEFAULT NULL,
  `sugared` tinyint(1) DEFAULT NULL,
  `fresh` tinyint(1) DEFAULT NULL,
  `treatment` enum('untreated','organic') CHARACTER SET ascii DEFAULT NULL,
  `preserving` set('brine','salt','confit','oil','syrup','smoked','dried','vinegar') CHARACTER SET ascii DEFAULT NULL,
  `processing` set('trimmed','shelled','boned','pitted','cleaned','drawn','dressed','denervated','disgorged') CHARACTER SET ascii DEFAULT NULL,
  `shape` enum('whole','powder','flakes','grated','shredded','slices','pieces','chopped','ground','minced','puree','diced','chips','tournedos','cut','ball','puffed') CHARACTER SET ascii DEFAULT NULL,
  `cooking` enum('raw','pre-cooked','half-cooked','cooked','grilled') CHARACTER SET ascii DEFAULT NULL,
  `packaging` enum('canned','frozen','jar','bag') CHARACTER SET ascii DEFAULT NULL,
  `profil` enum('omnivore','semi-vegetarian','vegetarian','vegan') CHARACTER SET ascii DEFAULT NULL,
  `shopping` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'Shopping category'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Ingredients definitions';

-- --------------------------------------------------------

--
-- Stand-in structure for view `ingredients_categorized`
-- (See below for the actual view)
--
CREATE TABLE `ingredients_categorized` (
`id` int(10) unsigned
,`singular` varchar(64)
,`plural` varchar(64)
,`profil` enum('omnivore','semi-vegetarian','vegetarian','vegan')
,`categories` mediumtext
);

-- --------------------------------------------------------

--
-- Table structure for table `ingredient_category`
--

CREATE TABLE `ingredient_category` (
  `ingredient` int(10) UNSIGNED NOT NULL,
  `category` smallint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ingredient_mapping`
--

CREATE TABLE `ingredient_mapping` (
  `source` tinyint(3) UNSIGNED NOT NULL COMMENT 'Source',
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'Original ingredient id',
  `ingredient` int(10) UNSIGNED NOT NULL COMMENT 'Ingredient id',
  `approximated` tinyint(1) DEFAULT NULL COMMENT 'Matching has been approximated'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Mapping with orignal recipe ingredient id';

-- --------------------------------------------------------

--
-- Table structure for table `key_image`
--

CREATE TABLE `key_image` (
  `source` tinyint(3) UNSIGNED NOT NULL,
  `key` varbinary(256) NOT NULL,
  `sha256` binary(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rawdata`
--

CREATE TABLE `rawdata` (
  `source` tinyint(3) UNSIGNED NOT NULL,
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `zdata` mediumblob DEFAULT NULL,
  `rejected` varchar(16) CHARACTER SET ascii DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dependencies` timestamp NULL DEFAULT NULL
  `processed` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Orignal recipe' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `source` tinyint(3) UNSIGNED NOT NULL COMMENT 'Source of recipe',
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'Recipe id as defined by source (if possible)',
  `rawid` bigint(20) UNSIGNED NOT NULL COMMENT 'Id of rawdata',
  `zxml` blob NOT NULL COMMENT 'Compressed xml data',
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'Name',
  `robot` set('TM5','TM31','TM21') CHARACTER SET ascii NOT NULL COMMENT 'Robot version',
  `time_robot` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Thermox time',
  `time_active` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Active time',
  `time_waiting` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Waiting time',
  `time_baking` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Baking time',
  `time_total` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Total time',
  `portion` smallint(5) UNSIGNED NOT NULL COMMENT 'Number of portions',
  `price` set('high','medium','low') CHARACTER SET ascii DEFAULT NULL COMMENT 'Price indication',
  `difficulty` set('advanced','medium','easy') CHARACTER SET ascii DEFAULT NULL COMMENT 'Difficulty indication',
  `meta` blob DEFAULT NULL COMMENT 'More informations (msgpack)',
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Updated time'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Recipes definitions' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_category`
--

CREATE TABLE `recipe_category` (
  `source` tinyint(3) UNSIGNED NOT NULL COMMENT 'Source',
  `recipe` bigint(20) UNSIGNED NOT NULL COMMENT 'Recipe',
  `category` smallint(5) UNSIGNED NOT NULL COMMENT 'Recipe category'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Join recipe and category';

-- --------------------------------------------------------

--
-- Table structure for table `recipe_ingredient`
--

CREATE TABLE `recipe_ingredient` (
  `source` tinyint(3) UNSIGNED NOT NULL COMMENT 'Source',
  `recipe` bigint(20) UNSIGNED NOT NULL COMMENT 'Recipe',
  `ingredient` int(10) UNSIGNED NOT NULL COMMENT 'Ingredient',
  `quantities` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'List of quantity/unit in json format'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='Join recipe and (not optional) ingredient';

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(16) CHARACTER SET ascii NOT NULL,
  `nickname` varchar(16) NOT NULL,
  `email` varchar(64) CHARACTER SET ascii DEFAULT NULL COMMENT 'e-mail'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Registered users';

-- --------------------------------------------------------

--
-- Table structure for table `user_favorite_recipe`
--

CREATE TABLE `user_favorite_recipe` (
  `user` varchar(16) CHARACTER SET ascii NOT NULL COMMENT 'User identifier',
  `source` tinyint(3) UNSIGNED NOT NULL COMMENT 'Source',
  `recipe` bigint(20) UNSIGNED NOT NULL COMMENT 'Recipe'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='User favorite recipe';

-- --------------------------------------------------------

--
-- Structure for view `ingredients_categorized`
--
DROP TABLE IF EXISTS `ingredients_categorized`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ingredients_categorized`  AS  select `ingredients`.`id` AS `id`,`ingredients`.`singular` AS `singular`,`ingredients`.`plural` AS `plural`,`ingredients`.`profil` AS `profil`,group_concat(distinct `categories_for_ingredients`.`key` separator ',') AS `categories` from ((`ingredients` left join `ingredient_category` on(`ingredients`.`id` = `ingredient_category`.`ingredient`)) left join `categories_for_ingredients` on(`ingredient_category`.`category` = `categories_for_ingredients`.`id`)) group by `ingredients`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories_for_ingredients`
--
ALTER TABLE `categories_for_ingredients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `categories_for_recipes`
--
ALTER TABLE `categories_for_recipes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_key` (`group`,`key`) USING BTREE,
  ADD KEY `image` (`image`);

--
-- Indexes for table `categories_for_shopping`
--
ALTER TABLE `categories_for_shopping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`sha256`),
  ADD KEY `derivative` (`derivative`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shopping` (`shopping`);

--
-- Indexes for table `ingredient_category`
--
ALTER TABLE `ingredient_category`
  ADD PRIMARY KEY (`ingredient`,`category`),
  ADD KEY `ingredient_category_ibfk_1` (`category`);

--
-- Indexes for table `ingredient_mapping`
--
ALTER TABLE `ingredient_mapping`
  ADD PRIMARY KEY (`source`,`id`) USING BTREE,
  ADD KEY `ingredient` (`ingredient`) USING BTREE;

--
-- Indexes for table `key_image`
--
ALTER TABLE `key_image`
  ADD PRIMARY KEY (`source`,`key`) USING BTREE,
  ADD KEY `sha256` (`sha256`);

--
-- Indexes for table `rawdata`
--
ALTER TABLE `rawdata`
  ADD PRIMARY KEY (`source`,`id`) USING BTREE,
  ADD KEY `updated` (`updated`),
  ADD KEY `dependencies` (`dependencies`),
  ADD KEY `processed` (`processed`),
  ADD KEY `rejected` (`rejected`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`source`,`id`) USING BTREE,
  ADD UNIQUE KEY `source_rawid` (`source`,`rawid`) USING BTREE,
  ADD KEY `robot` (`robot`),
  ADD KEY `price` (`price`),
  ADD KEY `difficulty` (`difficulty`),
  ADD KEY `portion` (`portion`),
  ADD KEY `time_active` (`time_active`),
  ADD KEY `time_total` (`time_total`);
ALTER TABLE `recipes` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `recipe_category`
--
ALTER TABLE `recipe_category`
  ADD PRIMARY KEY (`source`,`recipe`,`category`) USING BTREE,
  ADD KEY `category` (`category`) USING BTREE;

--
-- Indexes for table `recipe_ingredient`
--
ALTER TABLE `recipe_ingredient`
  ADD PRIMARY KEY (`source`,`recipe`,`ingredient`) USING BTREE,
  ADD KEY `recipe_ingredient_ibfk_2` (`ingredient`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_favorite_recipe`
--
ALTER TABLE `user_favorite_recipe`
  ADD PRIMARY KEY (`user`,`source`,`recipe`) USING BTREE,
  ADD KEY `source` (`source`,`recipe`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories_for_ingredients`
--
ALTER TABLE `categories_for_ingredients`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1323;

--
-- AUTO_INCREMENT for table `categories_for_shopping`
--
ALTER TABLE `categories_for_shopping`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories_for_recipes`
--
ALTER TABLE `categories_for_recipes`
  ADD CONSTRAINT `categories_for_recipes_ibfk_1` FOREIGN KEY (`image`) REFERENCES `key_image` (`sha256`) ON DELETE SET NULL;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`derivative`) REFERENCES `images` (`sha256`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD CONSTRAINT `ingredients_ibfk_1` FOREIGN KEY (`shopping`) REFERENCES `categories_for_shopping` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `ingredient_category`
--
ALTER TABLE `ingredient_category`
  ADD CONSTRAINT `ingredient_category_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories_for_ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ingredient_category_ibfk_2` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ingredient_mapping`
--
ALTER TABLE `ingredient_mapping`
  ADD CONSTRAINT `ingredient_mapping_ibfk_1` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `key_image`
--
ALTER TABLE `key_image`
  ADD CONSTRAINT `key_image_ibfk_1` FOREIGN KEY (`sha256`) REFERENCES `images` (`sha256`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recipes`
--
ALTER TABLE `recipes`
  ADD CONSTRAINT `recipes_ibfk_1` FOREIGN KEY (`source`,`rawid`) REFERENCES `rawdata` (`source`, `id`);

--
-- Constraints for table `recipe_category`
--
ALTER TABLE `recipe_category`
  ADD CONSTRAINT `recipe_category_ibfk_2` FOREIGN KEY (`source`,`recipe`) REFERENCES `recipes` (`source`, `id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recipe_category_ibfk_3` FOREIGN KEY (`category`) REFERENCES `categories_for_recipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recipe_ingredient`
--
ALTER TABLE `recipe_ingredient`
  ADD CONSTRAINT `recipe_ingredient_ibfk_1` FOREIGN KEY (`source`,`recipe`) REFERENCES `recipes` (`source`, `id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recipe_ingredient_ibfk_2` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_favorite_recipe`
--
ALTER TABLE `user_favorite_recipe`
  ADD CONSTRAINT `user_favorite_recipe_ibfk_1` FOREIGN KEY (`source`,`recipe`) REFERENCES `recipes` (`source`, `id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_favorite_recipe_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
